package com.banilaste.nfcompile.type;

import java.util.Observable;

public class Variable extends Observable {
	private Type type;
	private Object value;
	
	public Variable(Type type) {
		this(type, null);
	}
	
	public Variable(Type type, Object value) {
		this.type = type;
		this.value = value;
	}

	
	public Variable init() {
		this.value = type.instantiate();
		
		return this;
	}

	public Type getType() {
		return type;
	}

	public Object getValue() {
		return value;
	}


	public Variable castTo(Type targetType) {
		return new Variable(targetType, type.to(value, targetType));
	}

	public Variable setValue(Object value) {
		if (value != this.value) {
			this.value = value;
			
			this.notifyObservers(this);
		} else {
			this.value = value;
		}
		
		return this;
	}

	public Variable copy() {
		return new Variable(this.type, this.type.duplicate(this.value));
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer(this.type.toString());
		sb.append(" = ");
		sb.append(this.value);
		
		return sb.toString();
	}
}