package com.banilaste.nfcompile.type;

/**
 * Type for a variable
 * @author Clement
 *
 */
public abstract class Type {
	
	private String name;
	
	public Type(String name) {
		this.name = name;
	}

	public boolean equals(Object o) {
		if (o instanceof Type) {
			Type type = (Type) o;
			
			return type.getName().equals(this.name);
		}
		
		return false;
	}

	public boolean castableTo(Type type) {
		if (type.equals(this)) {
			return true;
		}
		
		return false;
	}
	
	public String getName() {
		return name;
	}

	public abstract Object instantiate();
	
	public Object duplicate(Object o) {
		return o;
	}

	/**
	 * Convert data to the given type, assuming it is already the right type
	 * @param value value
	 * @param targetType target type
	 * @return casted value
	 */
	public Object to(Object value, Type targetType) {
		return value;
	}
}