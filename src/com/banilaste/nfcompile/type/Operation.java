package com.banilaste.nfcompile.type;

import java.util.HashMap;

public class Operation {
	public static Object add(Object obj1, Object obj2){
		/*if ((obj1 instanceof Float && obj2 instanceof Number) ||
				(obj2 instanceof Float && obj1 instanceof Number)) {
			
			// If one of them is a float, the result is a float
			return (double) obj1 + (double) obj2;
		}
		
		else if (obj1 instanceof Number
				&& obj2 instanceof Number) {
			
			return (int) obj1 + (int) obj2;
		}*/
		
		return (float) obj1 + (float) obj2;
		//return null;
	}
	
	public static Object substract(Object obj1, Object obj2){
		return (float) obj1 - (float) obj2;
		/*if ((obj1 instanceof Float && obj2 instanceof Number) ||
				(obj2 instanceof Float && obj1 instanceof Number)) {
			
			// If one of them is a float, the result is a float
			return (double) obj1 - (double) obj2;
		}
		
		else if (obj1 instanceof Number
				&& obj2 instanceof Number) {
			
			return (int) obj1 - (int) obj2;
		}
		
		return null;*/
	}
	public static Object multiply(Object obj1, Object obj2){
		return (float) obj1 * (float) obj2;
		/*if ((obj1 instanceof Float && obj2 instanceof Number) ||
				(obj2 instanceof Float && obj1 instanceof Number)) {
			
			// If one of them is a float, the result is a float
			return (double) obj1 * (double) obj2;
		}
		
		else if (obj1 instanceof Number
				&& obj2 instanceof Number) {
			
			return (int) obj1 * (int) obj2;
		}
		
		return null;*/
	}
	
	public static Object divide(Object obj1, Object obj2) {
		return (float) obj1 / (float) obj2;
		/*if ((obj1 instanceof Float && obj2 instanceof Number) ||
				(obj2 instanceof Float && obj1 instanceof Number)) {
			
			// If one of them is a float, the result is a float
			return (double) obj1 / (double) obj2;
		}
		
		else if (obj1 instanceof Number
				&& obj2 instanceof Number) {
			
			return (int) obj1 / (int) obj2;
		}
		
		return null;*/
	}
	
	public static Variable arrayGet(Object input, Object index, int offset, Type type) {
		if (input instanceof Object[] && index instanceof Integer) {
			Object[] array = (Object[]) input;

			Object result = array[((Integer) index) - offset];
			
			if (result instanceof Variable) {
				return (Variable) result;
			} else {
				return new Variable(type, result);
			}
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static Object objectGet(Object input, Object object) {
		if (input instanceof HashMap) {
			HashMap<String, Object> map = (HashMap<String, Object>) input;
			return map.get(object);
		}
		
		return null;
	}
}
