package com.banilaste.nfcompile.type;

import java.util.HashMap;
import java.util.Iterator;

public class ObjectType extends Type {
	private HashMap<String, Type> keys;
	
	public ObjectType(String name, HashMap<String, Type> keys) {
		super(name);
		
		this.keys = keys;
	}
	
	public HashMap<String, Type> getKeys() {
		return keys;
	}

	public void setKeys(HashMap<String, Type> keys) {
		this.keys = keys;
	}

	@Override
	public Object instantiate() {
		HashMap<String, Variable> values = new HashMap<>();
		
		for (Iterator<String> iter = keys.keySet().iterator(); iter.hasNext();) {
			String key = iter.next();
			
			// Create variable for each
			values.put(key, new Variable(keys.get(key), keys.get(key).instantiate()));
		}
		
		return values;
	}
}