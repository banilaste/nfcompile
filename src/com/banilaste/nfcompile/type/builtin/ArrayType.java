package com.banilaste.nfcompile.type.builtin;

import com.banilaste.nfcompile.type.Type;
import com.banilaste.nfcompile.type.Variable;

public class ArrayType extends Type {
	private Type type;
	private int[] sizes, offsets;
	
	public ArrayType(Type type, int[] sizes, int offsets[]) {
		super(type.getName());
		
		this.type = type;
		this.offsets = offsets;
		this.sizes = sizes;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof ArrayType) {
			ArrayType type = (ArrayType) o;
			
			return type.getDimensions().length == this.sizes.length &&
					super.equals(o);
		}
		
		return false;
	}
	
	/**
	 * Return the type of an element of the array
	 * @return type
	 */
	public Type getBeside() {
		// If it is a 1 dimension array
		if (this.sizes.length == 1) {
			// A element should be of the type of the array
			return type;
		} else {
			int[] dims = new int[sizes.length - 1];
			int[] offsets = new int[this.offsets.length - 1];
			
			for (int i = 0; i < dims.length; i++) {
				dims[i] = sizes[i + 1];
				offsets[i] = this.offsets[i + 1];
			}
			
			return new ArrayType(type, dims, offsets);
		}
	}

	@Override
	public Object instantiate() {		
		return instantiate(this.sizes.length);
	}
	
	/**
	 * Generated array recursively
	 * @param dim dimension to create
	 * @return array of the dimension given
	 */
	private Object instantiate(int dim) {
		// If the dimension reach 0, we can instantiate basic type
		if (dim == 0) {
			return new Variable(type, type.instantiate());
		}

		// Otherwise we create the array
		Object[] objects = new Object[this.sizes[this.sizes.length - dim]];
		
		for (int i = 0; i < objects.length; i++) {
			objects[i] = instantiate(dim - 1);
		}
		
		return objects;
	}
	
	public Object duplicate(Object o) {
		if (o instanceof Object[]) {
			Object[] array = (Object[]) o;
			Object[] copy = new Object[array.length];
			
			for (int i = 0; i < copy.length; i ++) {
				copy[i] = this.duplicate(array[i]);
			}
			
			return copy;
		} else if (o instanceof Variable) {
			return ((Variable) o).copy();
		}
		
		System.err.println("[Array duplication] The object provided is not an array");
		return null;
	}
	
	public Type getType() {
		return type;
	}

	public int[] getDimensions() {
		return sizes;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		
		sb.append("tableau de ");
		sb.append(this.type.toString());
		
		return sb.toString();
	}

	public int getOffset() {
		return offsets[0];
	}

	public int getSize() {
		return sizes[0];
	}
	
}