package com.banilaste.nfcompile.type.builtin;

import com.banilaste.nfcompile.type.Type;

public class StringType extends Type {
	private final static String name = "\u0000string";
	public final static Type TYPE = new StringType();

	private StringType() {
		super(name);
	}

	@Override
	public Object instantiate() {
		return "";
	}

	@Override
	public Object to(Object value, Type targetType) {
		String str = (String) value;
		
		if (targetType instanceof IntegerType) {
			return Integer.parseInt(str);
		} else if (targetType instanceof FloatType) {
			return Float.parseFloat(str);
		} else if (targetType instanceof CharacterType) {
			return str.charAt(0);
		}
		
		return value;
	}
	
	public String toString() {
		return name;
	}
}
