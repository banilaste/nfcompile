package com.banilaste.nfcompile.type.builtin;

import com.banilaste.nfcompile.type.Type;
import com.banilaste.nfcompile.type.Variable;

public class BooleanType extends Type {
	private final static String name = "bool�en";
	public final static Type TYPE = new BooleanType();
	
	public final static Variable TRUE = new Variable(TYPE, 1);
	public final static Variable FALSE = new Variable(TYPE, 0);
	
	private BooleanType() {
		super(name);
	}

	@Override
	public Object instantiate() {
		return 0;
	}

	@Override
	public boolean castableTo(Type type) {
		return super.castableTo(type) || type instanceof IntegerType;
	}
	
	@Override
	public Object to(Object value, Type targetType) {
		// A boolean is already an integer :)
		if (targetType == FloatType.TYPE) {
			return (float) ((int) value);
		}
		
		return value;
	}
	
	public String toString() {
		return name;
	}
}
