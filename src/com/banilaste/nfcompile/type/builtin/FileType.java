package com.banilaste.nfcompile.type.builtin;

import com.banilaste.nfcompile.context.builtin.StreamReader;
import com.banilaste.nfcompile.type.Type;
import com.banilaste.nfcompile.type.Variable;

/**
 * Type used for reading file or console
 * @author Cl�ment
 *
 */
public class FileType extends Type {
	private final static String name = "fichier";
	public final static FileType TYPE = new FileType();
	
	public final static Variable CONSOLE = new Variable(TYPE).setValue(new StreamReader(System.in));
	
	private FileType() {
		super(name);
	}
	
	@Override
	public Object instantiate() {
		return null;
	}
	
	public String toString() {
		return name;
	}
}
