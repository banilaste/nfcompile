package com.banilaste.nfcompile.type.builtin;

import com.banilaste.nfcompile.type.Type;

public class CharacterType extends Type {
	private final static String name = "caract�re";
	public final static Type TYPE = new CharacterType();

	private CharacterType() {
		super(name);
	}
	
	@Override
	public boolean castableTo(Type type) {
		return super.castableTo(type) ||
				type instanceof IntegerType ||
				type instanceof StringType;
	}

	@Override
	public Object to(Object value, Type targetType) {
		char val = (char) value;

		if (targetType instanceof IntegerType) {
			return (int) val;
		} else if (targetType instanceof StringType) {
			return "" + val;
		}
		
		return val;
	}
	
	@Override
	public Object instantiate() {
		return '\0';
	}
	
	public String toString() {
		return name;
	}
}
