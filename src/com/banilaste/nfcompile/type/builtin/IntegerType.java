package com.banilaste.nfcompile.type.builtin;

import com.banilaste.nfcompile.type.Type;

public class IntegerType extends Type {
	private final static String name = "entier";
	public final static Type TYPE = new IntegerType();

	private IntegerType() {
		super(name);
	}
	

	@Override
	public boolean castableTo(Type type) {
		return super.castableTo(type) || type instanceof CharacterType || type instanceof FloatType;
	}


	@Override
	public Object instantiate() {
		return 0;
	}


	@Override
	public Object to(Object value, Type targetType) {
		int val = (int) value;
		
		if (targetType instanceof CharacterType) {
			return (char) val;
		} else if (targetType instanceof FloatType) {
			return (float) val;
		}
		
		return val;
	}

	
	public String toString() {
		return name;
	}
	
}
