package com.banilaste.nfcompile.type.builtin;

import com.banilaste.nfcompile.type.Type;

public class FloatType extends Type {
	private final static String name = "r�el";
	public final static Type TYPE = new FloatType();

	private FloatType() {
		super(name);
	}
	

	@Override
	public boolean castableTo(Type type) {
		return super.castableTo(type) || type instanceof IntegerType;
	}


	@Override
	public Object instantiate() {
		return 0;
	}


	@Override
	public Object to(Object value, Type targetType) {
		float val = (float) value;
		
		if (targetType instanceof IntegerType) {
			return (int) val;
		}
		
		return val;
	}
	
	public String toString() {
		return name;
	}
}
