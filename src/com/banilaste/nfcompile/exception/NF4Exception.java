package com.banilaste.nfcompile.exception;

import com.banilaste.nfcompile.code.CodeLine;
import com.banilaste.nfcompile.node.Node;

public class NF4Exception extends Exception {
	private static final long serialVersionUID = 1L;
	
	// TODO more exceptions
	@Deprecated
	public NF4Exception(Node n) {
		super("erreur ligne " + n.getLine() + " : " + n.toString());
	}
	
	@Deprecated
	public NF4Exception(CodeLine line) {
		super("erreur ligne " + line.getLineNumber());
	}
	
	public NF4Exception(int line, String message) {
		super("erreur ligne " + line + " : " + message);
	}
	
	public NF4Exception(Node node, String message) {
		this(node == null ? -1 : node.getLine(), message);
	}
}
