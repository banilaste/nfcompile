package com.banilaste.nfcompile.exception;

import com.banilaste.nfcompile.node.Node;
import com.banilaste.nfcompile.node.SeparatorNode;

public class UnexpectedException extends NF4Exception {
	private static final long serialVersionUID = 1L;

	public UnexpectedException(Node node) {
		this(node.getLine(), node instanceof SeparatorNode ?
						((SeparatorNode) node).getBind().toString() : node.toString());		
	}
	
	public UnexpectedException(int line, String expression) {
		super(line, "unexpected expression : " + expression);		
	}
}
