package com.banilaste.nfcompile.exception;

import com.banilaste.nfcompile.node.Node;

public class UnaffectableException extends NF4Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnaffectableException(Node source) {
		super(source.getLine(), source.toString() + " is no affectable");
	}
}
