package com.banilaste.nfcompile.exception;

import com.banilaste.nfcompile.node.Node;

public class UnknowAlgorithmException extends NF4Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnknowAlgorithmException(Node source, String name) {
		super(source, "unknown algorithm name : " + name);
	}
}
