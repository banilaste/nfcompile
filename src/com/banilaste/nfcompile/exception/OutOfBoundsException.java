package com.banilaste.nfcompile.exception;

import com.banilaste.nfcompile.node.Node;
import com.banilaste.nfcompile.type.builtin.ArrayType;

public class OutOfBoundsException extends NF4Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OutOfBoundsException(Node source, ArrayType type, int givenIndex) {
		super(source, givenIndex + " is not a valid index, use integer between " +
				type.getOffset() + "and" + (type.getSize() + type.getOffset()));
	}
}
