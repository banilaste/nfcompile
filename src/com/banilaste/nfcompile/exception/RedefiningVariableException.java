package com.banilaste.nfcompile.exception;

import com.banilaste.nfcompile.node.Node;

public class RedefiningVariableException extends NF4Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RedefiningVariableException(Node source, String name) {
		super(source, name + " is already defined");
	}

}
