package com.banilaste.nfcompile.exception;

import com.banilaste.nfcompile.node.Node;
import com.banilaste.nfcompile.type.Type;

public class InvalidTypeException extends NF4Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidTypeException(Node source, Type type) {
		super(source, "invalid given type : " + type.getName());
	}
	
	public InvalidTypeException(Node source, Type type, Type expected) {
		super(source, type.getName() + " can not be casted into " + expected.getName());
	}
}
