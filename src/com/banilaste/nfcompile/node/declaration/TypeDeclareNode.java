package com.banilaste.nfcompile.node.declaration;

import com.banilaste.nfcompile.NodeBind;
import com.banilaste.nfcompile.code.LinearNodeProvider;
import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.exception.UnexpectedException;
import com.banilaste.nfcompile.node.Node;
import com.banilaste.nfcompile.node.SeparatorNode;
import com.banilaste.nfcompile.node.block.Program;
import com.banilaste.nfcompile.node.variable.StringNode;
import com.banilaste.nfcompile.type.ObjectType;
import com.banilaste.nfcompile.type.Type;

/**
 * Declaration a type
 * @author Cl�ment
 *
 */
public class TypeDeclareNode extends Node {
	private Type type;
	
	
	@Override
	public SeparatorNode parse(LinearNodeProvider provider, Program program) throws NF4Exception {
		SeparatorNode sep;
		ObjectKeysNode types = new ObjectKeysNode();
		
		this.addChild(types);
		
		sep = types.parse(provider, program);
		
		// A type declare node ends with a parenthesis
		if (sep.getBind() != NodeBind.PARENTHESIS_END) {
			throw new UnexpectedException(sep);
		}
		
		// Only a separator should be parsed
		return super.parse(provider, program);
	}

	@Override
	public void check(AlgorithmContext ctx) throws NF4Exception {
		super.check(ctx);
		
		// The parent should be the name of the type
		if (this.getParent() instanceof StringNode) {
			StringNode parent = (StringNode) this.getParent();
			
			// There should be one child
			if (this.children().size() == 1 &&
					this.child(0) instanceof ObjectKeysNode) {
				
				ObjectKeysNode child = (ObjectKeysNode) this.child(0);

				type = new ObjectType(parent.getContent(), child.getKeys());
				return;
			}
		}
		
		// The program can't reach here if there is no issue 
		throw new NF4Exception(this);
	}
	
	/**
	 * Get type described by the node
	 * @return object type
	 */
	public Type getType() {
		return type;
	}
}