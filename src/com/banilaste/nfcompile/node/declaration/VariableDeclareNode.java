package com.banilaste.nfcompile.node.declaration;

import java.util.Iterator;

import com.banilaste.nfcompile.NodeBind;
import com.banilaste.nfcompile.code.LinearNodeProvider;
import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.exception.UnexpectedException;
import com.banilaste.nfcompile.node.Node;
import com.banilaste.nfcompile.node.SeparatorNode;
import com.banilaste.nfcompile.node.block.Program;
import com.banilaste.nfcompile.node.variable.StringNode;
import com.banilaste.nfcompile.type.Type;

public class VariableDeclareNode extends Node {
	private Type type;
	
	@Override
	public SeparatorNode parse(LinearNodeProvider provider, Program program) throws NF4Exception {
		// First parse strings
		SeparatorNode sep;
		
		do {
			// We are supposed to parse string nodes and a final type
			sep = super.parse(provider, program);
		} while(sep.getBind() == NodeBind.VARIABLE_SEPARATOR);
		
		return sep;
	}
	
	@Override
	public void check(AlgorithmContext ctx) throws NF4Exception {
		super.check(ctx);
		
		// No child (wtf?)
		if (this.children().size() < 1) {
			return;
		}
		
		Iterator<Node> iter = this.children().iterator();
		Node next = iter.next();
		
		while(iter.hasNext() && next instanceof StringNode) {
			// Only the last node should have a child
			if (next.children().size() > 0) {
				throw new UnexpectedException(next.child(0));
			}
			
			next = iter.next();
			
		}
		
		// If a string is the last node
		if (next instanceof StringNode) {
			// It should contains the type
			if (next.children().size() == 1) {
				type = ((TypeNode) next.child(0)).getType();
				return;
			}
			
			throw new UnexpectedException(this);
		} else {
			throw new UnexpectedException(this);
		}
		
		
	}

	public Type getType(AlgorithmContext ctx) {
		return type;
	}
	
	public String[] getNames() throws NF4Exception {
		if (this.children().size() == 0) {
			return new String[0];
		}

		String strings[] = new String[this.children().size()];
		
		Iterator<Node> iter = this.children().iterator();
		Node next;
		int index = 0;
		
		while(iter.hasNext() && index < strings.length) {
			next = iter.next();
			
			if (next instanceof StringNode) {
				strings[index] = ((StringNode) next).getContent();
			} else {
				throw new NF4Exception(this);
			}
			
			index += 1;
		}
		
		return strings;
	}
}