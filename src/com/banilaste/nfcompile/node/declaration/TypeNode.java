package com.banilaste.nfcompile.node.declaration;

import com.banilaste.nfcompile.NodeBind;
import com.banilaste.nfcompile.code.LinearNodeProvider;
import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.exception.UnexpectedException;
import com.banilaste.nfcompile.node.Node;
import com.banilaste.nfcompile.node.SeparatorNode;
import com.banilaste.nfcompile.node.ValuableNode;
import com.banilaste.nfcompile.node.block.Program;
import com.banilaste.nfcompile.node.variable.ArrayGetNode;
import com.banilaste.nfcompile.node.variable.StringNode;
import com.banilaste.nfcompile.type.Type;
import com.banilaste.nfcompile.type.builtin.ArrayType;

public class TypeNode extends Node {
	private boolean isArray = false;
	private Type type;
	
	@Override
	public SeparatorNode parse(LinearNodeProvider provider, Program program) throws NF4Exception {
		SeparatorNode sep;
		Node node;
		
		// First node shall be the type of an array indicator
		node = provider.next();
		
		if (node instanceof SeparatorNode) {
			sep = (SeparatorNode) node;
			
			if (sep.getBind() == NodeBind.TYPE_ARRAY) {
				this.isArray = true;

				// The next node shall be the brackets containing ranges
				// for array inside
				node = provider.next();
				
				if (node instanceof ArrayGetNode) {
					ArrayGetNode arrayGet = (ArrayGetNode) node;

					// We set them as declaring brackets
					arrayGet.setDeclaring(true);
					
					sep = node.parse(provider, program);
					this.addChild(node);
					
					// An array brackets end should have been parsed
					if (sep.getBind() != NodeBind.ARRAY_BRACKETS_END) {
						throw new UnexpectedException(sep);
					}
					
					// Then parse type name (we could use super.parse but it avoid
					// to have string childrens where it should not be)
					node = provider.next();

					// There is the same keyword as in a for-loop
					if (node instanceof SeparatorNode && ((SeparatorNode) node).getBind() == NodeBind.FOR_SEPARATOR) {
						node = provider.next();
					}
					
					if (node instanceof StringNode) {
						this.addChild(node);
						
						// Should parse a separator
						return super.parse(provider, program);
					}
				}
			}
		} else {
			// We add it directly
			this.addChild(node);
			
			// Should only parse a separator
			return super.parse(provider, program);
		}
		
		// If we get there it is because the node is not expected here
		throw new UnexpectedException(node);
	}

	@Override
	public void check(AlgorithmContext ctx) throws NF4Exception {
		// There must be only 2 parameters
		if (this.children().size() > (this.isArray ? 2 : 1)) {
			throw new UnexpectedException(this.children().get(this.isArray ? 2 : 1));
		}
		
		// Otherwise we get the type back
		if (isArray) {
			ArrayGetNode arrayGetNode = (ArrayGetNode) this.child(0);
			
			// Node not checked before
			arrayGetNode.check(ctx);
			
			int[] indexes = arrayGetNode.getIndexes();
			int[] offset, sizes;
			
			offset = new int[indexes.length / 2];
			sizes = new int[indexes.length / 2];
			
			if (indexes.length % 2 != 0) {
				throw new NF4Exception(this);
			}
			
			for (int i = 0; i < offset.length; i += 1) {
				sizes[i] = indexes[2 * i + 1] - indexes[2 * i] + 1;
				offset[i] = indexes[2 * i];
			}
			
			Type subType = ctx.getProgram().getType(ValuableNode.getString(this.child(1)));
			
			type = new ArrayType(subType, sizes, offset);
		} else {
			type = ctx.getProgram().getType(ValuableNode.getString(this.child(0)));
		}
	}

	public Type getType() {
		return type;
	}
}
