package com.banilaste.nfcompile.node.declaration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import com.banilaste.nfcompile.NodeBind;
import com.banilaste.nfcompile.code.LinearNodeProvider;
import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.exception.RedefiningVariableException;
import com.banilaste.nfcompile.exception.UnexpectedException;
import com.banilaste.nfcompile.node.Node;
import com.banilaste.nfcompile.node.SeparatorNode;
import com.banilaste.nfcompile.node.block.Program;
import com.banilaste.nfcompile.node.variable.StringNode;
import com.banilaste.nfcompile.type.Type;

/**
 * Node containing objects names (string) and their type
 * @author Cl�ment
 *
 */
public class ObjectKeysNode extends Node {
	private HashMap<String, Type> keys;
	private ArrayList<String> orderedNames;
	
	@Override
	public SeparatorNode parse(LinearNodeProvider provider, Program program) throws NF4Exception {
		SeparatorNode sep;

		do {
			sep = super.parse(provider, program);
		} while(sep.getBind() == NodeBind.VARIABLE_SEPARATOR);

		return sep;
	}

	@Override
	public void check(AlgorithmContext ctx) throws NF4Exception {
		super.check(ctx);
		
		Node next;
		keys = new HashMap<>();
		orderedNames = new ArrayList<>();
		
		for (Iterator<Node> iter = this.children().iterator(); iter.hasNext();) {
			next = iter.next();

			// Children shall all be StringNode with type beside
			if (next instanceof StringNode) {
				String name = ((StringNode) next).getContent();

				// The node shall have only one child : it's type
				if (next.children().size() == 1 && next.child(0) instanceof TypeNode) {
					TypeNode type = (TypeNode) next.child(0);

					if (!keys.containsKey(name)) {
						keys.put(name, type.getType());
						orderedNames.add(name);
					} else {
						// redefine existing var
						throw new RedefiningVariableException(this, name);
					}
				}

			} else {
				throw new UnexpectedException(next);
			}
		}
	}

	public HashMap<String, Type> getKeys() {
		return keys;
	}

	public ArrayList<String> getOrderedNames() {
		return orderedNames;
	}
	
	public Type getChildType(int index) {
		return keys.get(orderedNames.get(index));
	}
	
	public void empty() {
		keys = new HashMap<>();
		orderedNames = new ArrayList<>();
	}
}
