package com.banilaste.nfcompile.node;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Observable;

import com.banilaste.nfcompile.Parsable;
import com.banilaste.nfcompile.code.LinearNodeProvider;
import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.node.block.Program;
import com.banilaste.nfcompile.type.Variable;

/**
 *	Basic node with children and a parent node
 */
public abstract class Node extends Observable implements Parsable {
	private int line;
	private LinkedList<Node> childrens;
	protected Node parent;
	
	public Node() {
		this.line = -1;
		this.childrens = new LinkedList<Node>();
		this.parent = null;
	}
	
	@Override
	public SeparatorNode parse(LinearNodeProvider provider, Program program) throws NF4Exception {
		// If no line has been given yet
		if (this.line == -1) {
			this.line = provider.getLine();
		}
		
		// We fetch next node
		Node node = provider.next();
		
		// If that's a separator
		if (node instanceof SeparatorNode) {
			// we no longer need it in a simple node
			return (SeparatorNode) node;
		}
		
		// Otherwise, with regular nodes
		else {
			// We add to children
			this.addChild(node);

			// And let them parse the remaining data
			return node.parse(provider, program);
		}
	}
	
	/**
	 * Assert code validity inside node
	 * @throws NF4Exception
	 */
	public void check(AlgorithmContext ctx) throws NF4Exception {
		// A node is valid only if all of it's children are valid
		Iterator<Node> iter = this.children().iterator();
		
		while(iter.hasNext()) {
			iter.next().check(ctx);
		}
	}
	
	/**
	 * Emulate the behavior of the node
	 * @param instance instance of the algorithm, containing variables
	 * @return variable returned or null
	 * @throws NF4Exception 
	 */
	public Variable simule(AlgorithmInstance instance) throws NF4Exception {
		return null;
	}
	
	public void addChild(Node node) {
		this.childrens.add(node);
		node.setParent(this);
	}
	
	public int getLine() {
		return line;
	}
	
	public void setLine(int line) {
		this.line = line;
	}
	
	public Node getParent() {
		return parent;
	}

	public LinkedList<Node> children() {
		return childrens;
	}

	public Node child(int index) {
		return this.children().get(index);
	}
	
	public void setParent(Node parent) {
		this.parent = parent;
	}
	
	public String toString() {
		return toString(0);
	}
	
	public String toString(int level) {
		return toString(level, null);
	}
	
	public String toString(int level, String label) {
		String tab = new String(new char[level]).replace("\0", "  ");;
		Iterator<Node> iter = this.children().iterator();
		StringBuffer buffer = new StringBuffer(tab);
		
		buffer.append(this.getClass().getSimpleName());
		
		if (label != null && label.length() > 0) {
			buffer.append(" : ");
			buffer.append(label);
		}
		
		buffer.append(" {");
		
		if (iter.hasNext()) {
			buffer.append("\n");
			buffer.append(iter.next().toString(level + 1));
		}
		
		while (iter.hasNext()) {
			buffer.append("\n");
			buffer.append(iter.next().toString(level + 1));
		}
		
		if (this.childrens.size() > 0) {
			buffer.append(tab);
		}
		
		buffer.append("}\n");
		
		return buffer.toString();
	}
}