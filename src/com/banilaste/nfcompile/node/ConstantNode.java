package com.banilaste.nfcompile.node;

import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.type.Type;
import com.banilaste.nfcompile.type.Variable;

public class ConstantNode extends ValuableNode {
	private Variable variable;
	
	public ConstantNode(Variable constant) {
		super();
		
		variable = constant;
	}

	@Override
	public Type getType() {
		return variable.getType();
	}
	
	public String toString(int level) {
		return this.toString(level, variable.toString());
	}

	@Override
	public Variable simule(AlgorithmInstance ctx) {
		return variable;
	}
}
