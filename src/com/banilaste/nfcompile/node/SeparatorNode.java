package com.banilaste.nfcompile.node;

import com.banilaste.nfcompile.NodeBind;

/**
 * Node pointing a separator useful to some node or to exit
 * some node. The given object in constructor describe which
 * node was parsed.
 * @author Clement
 *
 */
public class SeparatorNode extends Node {
	public final static SeparatorNode LINE_END = new SeparatorNode(NodeBind.LINE_END);
	public final static Node FILE_END = new SeparatorNode(NodeBind.FILE_END);

	private NodeBind bind;
	
	public SeparatorNode(NodeBind bind) {
		this.bind = bind;
	}

	public NodeBind getBind() {
		return bind;
	}
	
	public String toString(int level) {
		return toString(level, this.bind.toString());
	}
}