package com.banilaste.nfcompile.node.operation;

import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.node.ValuableNode;
import com.banilaste.nfcompile.type.Type;
import com.banilaste.nfcompile.type.Variable;
import com.banilaste.nfcompile.type.builtin.FloatType;
import com.banilaste.nfcompile.type.builtin.IntegerType;

public abstract class NumericalOperationNode extends OperationNode {
	
	@Override
	public Variable simule(AlgorithmInstance ctx) throws NF4Exception {
		// Return type (for casting before)
		Type returnType = this.getType();
		Variable result = null;
		Variable var1 = this.child(0).simule(ctx).castTo(returnType).castTo(FloatType.TYPE);

		if (this.children().size() == 2) {
			try {
			result = operate(var1, this.child(1).simule(ctx).castTo(returnType).castTo(FloatType.TYPE), null);
			}catch (Exception e) {
				System.err.println(this);
				System.err.println(this.getLine());
			}
		} else {
			result = operate(var1, null, null);
		}
		
		if (getType() instanceof IntegerType) {
			result.setValue((int) ((float) result.getValue()));
		}
		
		return result;
	}

	@Override
	public void check(AlgorithmContext ctx) throws NF4Exception {
		super.check(ctx);
		
		Type type = ValuableNode.getType(this, 0);

		// 2 integers makes one
		if (type == IntegerType.TYPE &&
				ValuableNode.getType(this, 1) == IntegerType.TYPE) {

			returnType = IntegerType.TYPE;
		} else {	
			// or it become a float
			returnType = FloatType.TYPE;
		}
	}

}
