package com.banilaste.nfcompile.node.operation;

import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.exception.InvalidTypeException;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.exception.UnaffectableException;
import com.banilaste.nfcompile.exception.UnexpectedException;
import com.banilaste.nfcompile.node.ValuableNode;
import com.banilaste.nfcompile.type.Variable;

public class AffectNode extends OperationNode {

	@Override
	public void check(AlgorithmContext ctx) throws NF4Exception {
		super.check(ctx);

		if (!ValuableNode.isAffectable(this.child(0))) {
			throw new UnaffectableException(this.child(0));
		}
		
		// Both node need to accept a value
		if (this.child(1) instanceof ValuableNode) {
			ValuableNode target = (ValuableNode) this.child(0);
			ValuableNode source = (ValuableNode) this.child(1);

			try {
				// If types are compatible
				if (target.getType().castableTo(source.getType())) {
					returnType = target.getType();

					return;
				}
			} catch(NullPointerException e) {
				System.out.println(target.getType());
			}
			
			throw new InvalidTypeException(this, source.getType(), target.getType());
		}

		throw new UnexpectedException(this.child(1));

	}

	@Override
	public int getPriority() {
		return -100;
	}

	@Override
	public Variable operate(Variable var1, Variable var2, AlgorithmContext ctx) {
		var1.setValue(var2.castTo(var1.getType()).getValue());

		return var1;
	}


}
