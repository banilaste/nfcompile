package com.banilaste.nfcompile.node.operation;

import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.exception.UnexpectedException;
import com.banilaste.nfcompile.node.Node;
import com.banilaste.nfcompile.node.ValuableNode;
import com.banilaste.nfcompile.node.variable.OperationContainingNode;
import com.banilaste.nfcompile.type.Variable;

public abstract class OperationNode extends ValuableNode {

	/**
	 * Return operator priority. High priority node goes
	 * low in the tree
	 * @return priority
	 */
	public abstract int getPriority();
	private boolean parentSet = false;

	/**
	 * Place the node in the right place
	 * @param parent parent node
	 * @param overrideParents take parent operation priority in account
	 */
	public void setParent(Node parent) {
		super.setParent(parent);

		// We loop only once (allow to last operations to be higher
		// without stack overflow)
		if (parentSet) {
			return;
		}

		// While the parent describe a variable (brackets, property..)
		while ((this.parent instanceof ValuableNode && !(this.parent instanceof OperationNode)) ||
				(this.parent instanceof OperationNode &&
				((OperationNode) this.parent).getPriority() >= this.getPriority())) {
			// If our parent contains operations and is able to stop current
			if (this.parent instanceof OperationContainingNode) {
				OperationContainingNode containerNode = (OperationContainingNode) this.parent;

				// We stop for a parenthesis because parenthesis are cute :3
				if (!containerNode.canStealParent()) {
					break;
				}
			}
			
			// The node take it's place
			this.stealParenting();
		}

		parentSet = true;
	}

	/**
	 * Run the operation
	 * @param var1 first operand
	 * @param var2 second operand
	 * @param ctx context in which the operation shall be done
	 * @return result of the operation
	 * @throws NF4Exception 
	 */
	public abstract Variable operate(Variable var1, Variable var2, AlgorithmContext ctx) throws NF4Exception;

	public Variable simule(AlgorithmInstance inst) throws NF4Exception {
		return operate(
				this.child(0).simule(inst),
				this.child(1).simule(inst),
				inst.getContext()
				);
	}

	@Override
	public void check(AlgorithmContext ctx) throws NF4Exception {
		super.check(ctx);
		
		if (this.children().size() > 2) {
			throw new UnexpectedException(this.child(2));
		}
	}
}