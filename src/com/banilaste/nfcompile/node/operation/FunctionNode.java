package com.banilaste.nfcompile.node.operation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import com.banilaste.nfcompile.NodeBind;
import com.banilaste.nfcompile.code.LinearNodeProvider;
import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.exception.UnexpectedException;
import com.banilaste.nfcompile.exception.UnknowAlgorithmException;
import com.banilaste.nfcompile.node.Node;
import com.banilaste.nfcompile.node.SeparatorNode;
import com.banilaste.nfcompile.node.SubAlgorithmNode;
import com.banilaste.nfcompile.node.ValuableNode;
import com.banilaste.nfcompile.node.block.Program;
import com.banilaste.nfcompile.node.variable.OperationContainingNode;
import com.banilaste.nfcompile.node.variable.VariableTravelNode;
import com.banilaste.nfcompile.type.Type;
import com.banilaste.nfcompile.type.Variable;

public class FunctionNode extends ValuableNode implements OperationContainingNode {
	private LinkedList<Node> inputsParameters, outputParameters;
	private SubAlgorithmNode function;

	private boolean insideParsed;
	protected int childrens;

	@Override
	public SeparatorNode parse(LinearNodeProvider provider, Program program) throws NF4Exception {
		SeparatorNode sep;

		// We take our parent place (which must return a function name)
		if (this.getParent() instanceof ValuableNode) {
			this.stealParenting();
		} else {
			throw new UnexpectedException(this);
		}

		insideParsed = false;

		do {
			sep = super.parse(provider, program);

			if (sep.getBind() == NodeBind.FUNCTION_IO_SPLIT) {
				// We add input/output separator to children for further verification
				this.addChild(sep);
			}
		} while(sep.getBind() == NodeBind.FUNCTION_IO_SPLIT || sep.getBind() == NodeBind.VARIABLE_SEPARATOR);

		// Saving parameters (should be equals to children count when checking)
		childrens = this.children().size();

		if (sep.getBind() != NodeBind.PARENTHESIS_END) {
			throw new UnexpectedException(sep);
		}

		// Flag inside parsed as true
		insideParsed = true;

		return super.parse(provider, program);
	}


	@Override
	public void check(AlgorithmContext ctx) throws NF4Exception {
		super.check(ctx);

		// There is only one case where a node can be outside parenthesis
		if (childrens != this.children().size()) {

			if (this.child(childrens) instanceof VariableTravelNode) {
				((ValuableNode) this.child(childrens)).stealParenting();
			} else {
				// TODO check func().test
				throw new UnexpectedException(this.child(childrens));
			}
		}

		// Function case
		inputsParameters = new LinkedList<>();
		outputParameters = new LinkedList<>();

		// Input parameters verification
		Node next = null;
		Iterator<Node> iter = this.children().iterator();

		// Getting back function (first child value)
		next = iter.next();
		function = ctx.getProgram().getFunction(ValuableNode.getString(next));

		// Function does not exists
		if (function == null) {
			throw new UnknowAlgorithmException(this, ValuableNode.getString(next));
		}

		ArrayList<Type> params = new ArrayList<>();

		// Fetching input parameters type
		while (iter.hasNext()) {
			next = iter.next();

			if (next instanceof SeparatorNode) {
				break;
			}

			inputsParameters.add(next);
			params.add(((ValuableNode) next).getType());
		}

		// Check it's validity
		function.getContext().checkInputParameters(params);

		params.clear();

		// Then do the same for output parameters
		while (iter.hasNext()) {
			next = iter.next();

			params.add(((ValuableNode) next).getType());
			outputParameters.add(next);
		}

		function.getContext().checkOutputParameters(params);

		if (outputParameters.size() == 0 &&
				function.getContext().getOutputParameters().getOrderedNames().size() == 1) {

			returnType = function.getContext().getOutputParameters().getChildType(0);
		} else {
			returnType = null;
		}
	}



	@Override
	public Variable simule(AlgorithmInstance instance) throws NF4Exception {
		AlgorithmInstance subInstance = function.newInstance();
		LinkedList<Variable> inputs = new LinkedList<>(),
				outputs = new LinkedList<>();

		for (Iterator<Node> iter = inputsParameters.iterator(); iter.hasNext();) {
			inputs.add(iter.next().simule(instance));
		}

		for (Iterator<Node> iter = outputParameters.iterator(); iter.hasNext();) {
			outputs.add(iter.next().simule(instance));
		}

		subInstance.parameters(inputs, outputs);

		return function.simule(subInstance);
	}


	@Override
	public boolean canStealParent() {
		return insideParsed;
	}


}
