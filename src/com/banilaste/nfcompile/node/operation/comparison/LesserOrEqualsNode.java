package com.banilaste.nfcompile.node.operation.comparison;

import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.type.Variable;
import com.banilaste.nfcompile.type.builtin.BooleanType;

public class LesserOrEqualsNode extends NumericalComparisonNode {
	@Override
	public Variable operate(Variable var1, Variable var2, AlgorithmContext ctx) {
		return (float) var1.getValue() <= (float) var2.getValue() ? BooleanType.TRUE : BooleanType.FALSE;
	}
}
