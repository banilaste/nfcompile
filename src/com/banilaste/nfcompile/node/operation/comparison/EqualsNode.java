package com.banilaste.nfcompile.node.operation.comparison;

import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.type.Variable;
import com.banilaste.nfcompile.type.builtin.BooleanType;

public class EqualsNode extends ComparisonNode {

	@Override
	public Variable operate(Variable var1, Variable var2, AlgorithmContext ctx) {
		return var1.getValue().equals(var2.getValue()) ? BooleanType.TRUE : BooleanType.FALSE;
	}
}
