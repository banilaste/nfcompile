package com.banilaste.nfcompile.node.operation.comparison;

import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.exception.InvalidTypeException;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.node.ValuableNode;
import com.banilaste.nfcompile.node.operation.OperationNode;
import com.banilaste.nfcompile.type.Type;
import com.banilaste.nfcompile.type.Variable;
import com.banilaste.nfcompile.type.builtin.BooleanType;

public abstract class ComparisonNode extends OperationNode {

	@Override
	public int getPriority() {
		return 10;
	}
	
	@Override
	public void check(AlgorithmContext ctx) throws NF4Exception {
		super.check(ctx);

		Type t1 = ValuableNode.getType(this, 0);
		Type t2 = ValuableNode.getType(this, 1);
		
		// Incompatible type to compare
		if (!t1.castableTo(t2) && !t2.castableTo(t1)) {
			
			throw new InvalidTypeException(this, t1, t2);
		}
	}

	public Type getType() {
		return BooleanType.TYPE;
	}

	@Override
	public Variable simule(AlgorithmInstance instance) throws NF4Exception {
		Variable var1 = this.child(0).simule(instance);
		Variable var2 = this.child(1).simule(instance);
		
		Type targetType = var2.getType();
		
		// At least a type is castable to another
		if (var1.getType().castableTo(targetType)) {
			var1 = var1.castTo(targetType);
		} else {
			var2 = var2.castTo(var1.getType());
		}

		return operate(var1, var2, instance.getContext());
	}
}
