package com.banilaste.nfcompile.node.operation.comparison;

import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.type.Variable;
import com.banilaste.nfcompile.type.builtin.FloatType;

public abstract class NumericalComparisonNode extends ComparisonNode {
	@Override
	public Variable simule(AlgorithmInstance ctx) throws NF4Exception {
		Variable result;
		Variable var1 = this.child(0).simule(ctx).castTo(FloatType.TYPE);
	
		if (this.children().size() == 2) {
			result = operate(var1, this.child(1).simule(ctx).castTo(FloatType.TYPE), null);
		} else {
			result = operate(var1, null, null);
		}
		
		return result;
	}
}
