package com.banilaste.nfcompile.node.operation;

import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.type.Variable;

public class DivisionNode extends NumericalOperationNode {

	@Override
	public int getPriority() {
		return 50;
	}

	@Override
	public Variable operate(Variable var1, Variable var2, AlgorithmContext ctx) {
		return new Variable(this.getType(), (float) var1.getValue() / (float) var2.getValue());
	}

}
