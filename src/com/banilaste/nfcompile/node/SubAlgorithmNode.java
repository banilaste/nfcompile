package com.banilaste.nfcompile.node;

import com.banilaste.nfcompile.NodeBind;
import com.banilaste.nfcompile.code.LinearNodeProvider;
import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.exception.UnexpectedException;
import com.banilaste.nfcompile.node.block.BlockNode;
import com.banilaste.nfcompile.node.block.Program;
import com.banilaste.nfcompile.node.declaration.ObjectKeysNode;
import com.banilaste.nfcompile.node.variable.StringNode;
import com.banilaste.nfcompile.type.Variable;

public class SubAlgorithmNode extends BlockNode {
	protected String name;
	protected AlgorithmContext context;
	
	@Override
	public SeparatorNode parse(LinearNodeProvider provider, Program program) throws NF4Exception {
		// The name shall be parsed first
		Node node = provider.next();
		
		if (!(node instanceof StringNode)) {
			throw new UnexpectedException(node);
		} else {
			StringNode strNode = (StringNode) node;
			strNode.parse(provider, program);
			
			this.name = strNode.getContent();
		}
		
		// We can register the algorithm to the program
		program.addFunction(this);
		
		// Context for algorithm (contains custom variables and types)
		context = new AlgorithmContext(program);
			
		node = provider.next();
		
		// Parse every node until the end
		while (node instanceof SeparatorNode) {
			NodeBind bind = ((SeparatorNode) node).getBind();
			
			switch(bind) {
			case TYPES_START: // types definition
				// There should be a line end
				if (provider.next() != SeparatorNode.LINE_END) {
					throw new UnexpectedException(provider.getLast());
				}
				
				node = context.getProgram().parseTypes(provider, context);
				break;
			case VARIABLES_START: // variables declaration
				// There should be a line end
				if (provider.next() != SeparatorNode.LINE_END) {
					throw new UnexpectedException(provider.getLast());
				}
				
				node = context.parseVariables(provider, program);
				break;
			case INPUT_PARAMETER:
				ObjectKeysNode params = new ObjectKeysNode();
				
				node = params.parse(provider, program);
				params.check(context);
				
				context.setInputParameters(params);
				break;
			case OUTPUT_PARAMETER:
				ObjectKeysNode outParams = new ObjectKeysNode();
				
				node = outParams.parse(provider, program);
				outParams.check(context);
				
				context.setOutputParameters(outParams);
				break;
			case LINE_END: // end of line
				node = provider.next();
				break;
			case INSTRUCTIONS_START: // instructions
				context.parsingDone();
				context.setStartingLine(provider.getLine());
				
				node = super.parse(provider, program);
				break;
			case END: // end of algorithm
				return SeparatorNode.LINE_END;			
			
			default:
				// No other separator shall be found here
				// The algorithm require to be ended properly
				throw new UnexpectedException(node);
			}
		}

		// We do not expect a regular node here
		throw new UnexpectedException(node);
	}

	@Override
	public void check(AlgorithmContext ctx) throws NF4Exception {
		if (ctx != null) {
			throw new NF4Exception(this);
		}
		
		super.check(this.getContext());
	}

	@Override
	public Variable simule(AlgorithmInstance instance) throws NF4Exception {
		// Notitify observers that an new instance is created
		this.notifyObservers(instance);
		
		// Simule content with the given instance
		super.simule(instance);

		// Then notify again that the instance is over
		this.notifyObservers(null);
		
		// Apply output variables
		return instance.applyOutput();
	}

	@Override
	public String toString(int level) {
		return toString(level, this.name);
	}

	public AlgorithmContext getContext() {
		return context;
	}

	public String getName() {
		return name;
	}

	public AlgorithmInstance newInstance() {
		return new AlgorithmInstance(context);
	}
	
	
}