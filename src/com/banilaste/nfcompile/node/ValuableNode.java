package com.banilaste.nfcompile.node;

import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.node.operation.FunctionNode;
import com.banilaste.nfcompile.node.operation.OperationNode;
import com.banilaste.nfcompile.node.variable.StringNode;
import com.banilaste.nfcompile.node.variable.VariableTravelNode;
import com.banilaste.nfcompile.type.Type;
import com.banilaste.nfcompile.type.Variable;

/**
 * Node returning a typed value
 */
public abstract class ValuableNode extends Node {
	protected Type returnType;
	
	
	@Override
	public abstract Variable simule(AlgorithmInstance instance) throws NF4Exception; // force child to return a value :)

	/**
	 * Make current node take it's parent place
	 */
	public void stealParenting() {
		Node parent = this.parent;
		
		// It take it's parent place
		this.parent = parent.getParent();
		this.parent.children().add(this);
		this.parent.children().remove(parent);

		// Et on met notre ancien parent dessous
		parent.setParent(this);
		parent.children().remove(this);
		
		// Then we give all of our children (ascending come with a cost :p)
		this.children().forEach(ch -> {
			parent.addChild(ch);
		});
		
		// Which we can remove after
		this.children().clear();
		
		// To add our new children
		this.children().add(parent);
	}
	
	/**
	 * Type returned by the node
	 */
	public Type getType() {
		return returnType;
	}
	
	/**
	 * Return whether the given node can be given a value
	 * @param node node to be tested
	 * @return 
	 */
	public static boolean isAffectable(Node node) {
		return node instanceof ValuableNode &&
				(!(node instanceof OperationNode) || node instanceof VariableTravelNode)
				&& !(node instanceof ConstantNode);
	}

	public static boolean isType(Node node, Type type) {
		if (node instanceof ValuableNode) {
			return type.equals(((ValuableNode) node).getType());
		}

		return false;
	}

	public static boolean childIsType(Node node, int index, Type type) {
		return isType(node.child(index), type);
	}
	
	public static Type getType(Node node, int childId) {
		return getType(node.child(childId));
	}
	
	public static Type getType(Node node) {
		if (node instanceof ValuableNode) {
			ValuableNode vNode = (ValuableNode) node;

			return vNode.getType();
		}

		return null;
	}

	public static String getString(Node child) {
		if (child instanceof StringNode) {
			return ((StringNode) child).getContent();
		}
		
		return null;
	}

	public static String getString(FunctionNode node, int i) {
		return getString(node.child(i));
	}
	
}