package com.banilaste.nfcompile.node;

import com.banilaste.nfcompile.code.LinearNodeProvider;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.node.block.Program;

public class AlgorithmNode extends SubAlgorithmNode {
	@Override
	public SeparatorNode parse(LinearNodeProvider provider, Program program) throws NF4Exception {
		// We register the main algorithm to the program
		program.setMain(this);
		
		return super.parse(provider, program);
	}
}