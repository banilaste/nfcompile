package com.banilaste.nfcompile.node.variable;

import java.util.Iterator;

import com.banilaste.nfcompile.NodeBind;
import com.banilaste.nfcompile.code.LinearNodeProvider;
import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.exception.InvalidTypeException;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.exception.OutOfBoundsException;
import com.banilaste.nfcompile.exception.UnaffectableException;
import com.banilaste.nfcompile.exception.UnexpectedException;
import com.banilaste.nfcompile.node.Node;
import com.banilaste.nfcompile.node.SeparatorNode;
import com.banilaste.nfcompile.node.ValuableNode;
import com.banilaste.nfcompile.node.block.Program;
import com.banilaste.nfcompile.type.Operation;
import com.banilaste.nfcompile.type.Type;
import com.banilaste.nfcompile.type.Variable;
import com.banilaste.nfcompile.type.builtin.ArrayType;
import com.banilaste.nfcompile.type.builtin.IntegerType;

public class ArrayGetNode extends VariableTravelNode implements OperationContainingNode {
	/**
	 *	Boolean indicating whether the brackets are
	 *	used in a declaration context or in a 
	 *	instruction context
	 */
	private boolean isDeclaration = false;
	private Type type;
	private ArrayType sourceType;
	private boolean insideParsed;
	private int[] indexes;
	private int offset;

	@Override
	public SeparatorNode parse(LinearNodeProvider provider, Program program) throws NF4Exception {
		SeparatorNode sep;

		insideParsed = false;

		// We parse every indexes
		do {
			sep = super.parse(provider, program);
		} while(sep.getBind() == NodeBind.VARIABLE_SEPARATOR ||
				(this.isDeclaration && sep.getBind() == NodeBind.ARRAY_DECLARE_SEPARATOR));

		// The last separator should be ]
		if (sep.getBind() != NodeBind.ARRAY_BRACKETS_END) {
			throw new UnexpectedException(sep);
		}

		insideParsed = true;

		if (this.isDeclaration) {
			// We stop there for declaration
			return sep;
		} else {
			// Or we parse the following as a regular node
			return super.parse(provider, program);
		}
	}

	@Override
	public void check(AlgorithmContext ctx) throws NF4Exception {
		// Declaration
		if (this.isDeclaration) {
			indexes = new int[this.children().size()];

			Iterator<Node> iter = this.children().iterator();
			Node next;

			for (int i = 0; i < indexes.length && iter.hasNext(); i++) {
				next = iter.next();

				if (next instanceof ValuableNode) {
					// Shall not use an instance because it is constant
					// therefore if there is an error it's the user's fault
					Variable returnValue = ((ValuableNode) next).simule(null);

					if (returnValue.getType() == IntegerType.TYPE) {
						indexes[i] = (int) returnValue.getValue();
					} else {
						throw new InvalidTypeException(next, returnValue.getType(), IntegerType.TYPE);
					}
				}
			} 

		} else {
			// Only check for the operator
			super.check(ctx);

			if (!ValuableNode.childIsType(this, 1, IntegerType.TYPE)) {
				throw new InvalidTypeException(this.child(1), ValuableNode.getType(this, 1), IntegerType.TYPE);
			}

			if (!ValuableNode.isAffectable(this.child(0))) {
				throw new UnaffectableException(this.child(0));
			}


			ValuableNode variable = (ValuableNode) this.child(0);
			Type type = variable.getType();

			if (type instanceof ArrayType) {
				sourceType = (ArrayType) type;

				this.offset = sourceType.getOffset();
				this.type = sourceType.getBeside();
				return;
			}

			// Not an array
			throw new InvalidTypeException(variable, type);
		}
	}

	public int[] getIndexes() {
		return indexes;
	}

	@Override
	public Type getType() {
		return type;
	}

	public void setDeclaring(boolean decl) {
		this.isDeclaration = decl;
	}

	@Override
	public Variable operate(Variable var1, Variable var2, AlgorithmContext ctx) throws NF4Exception {
		try {
			return Operation.arrayGet(var1.getValue(), var2.getValue(), offset, getType());
		} catch(IndexOutOfBoundsException e) {
			throw new OutOfBoundsException(this, sourceType, (Integer) var2.getValue());
		}
	}

	@Override
	public boolean canStealParent() {
		return insideParsed;
	}

}