package com.banilaste.nfcompile.node.variable;

import com.banilaste.nfcompile.NodeBind;
import com.banilaste.nfcompile.code.LinearNodeProvider;
import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.exception.UnexpectedException;
import com.banilaste.nfcompile.node.SeparatorNode;
import com.banilaste.nfcompile.node.ValuableNode;
import com.banilaste.nfcompile.node.block.Program;
import com.banilaste.nfcompile.type.Variable;

public class ParenthesisNode extends ValuableNode implements OperationContainingNode {
	private boolean insideParsed;
	
	@Override
	public SeparatorNode parse(LinearNodeProvider provider, Program program) throws NF4Exception {
		SeparatorNode sep;
		
		insideParsed = false;
		
		sep = super.parse(provider, program);

		if (sep.getBind() != NodeBind.PARENTHESIS_END) {
			throw new UnexpectedException(sep);
		}
		
		// Flag inside parsed as true
		insideParsed = true;
		
		return super.parse(provider, program);
	}


	@Override
	public void check(AlgorithmContext ctx) throws NF4Exception {
		super.check(ctx);
		
		// There is only one case where a node can be outside parenthesis
		if (this.children().size() > 1) {
			// In which can he is allowed to take parenting
			if (this.children().getLast() instanceof VariableTravelNode) {
				((ValuableNode) this.children().getLast()).stealParenting();
			}
			
			throw new UnexpectedException(this.children().getLast());
		}

		
		if (this.children().size() == 0) {
			throw new UnexpectedException(this);
		}
		
		returnType = ValuableNode.getType(this, 0);
	}

	@Override
	public Variable simule(AlgorithmInstance instance) throws NF4Exception {
		return this.child(0).simule(instance);
	}


	@Override
	public boolean canStealParent() {
		return insideParsed;
	}
	
}