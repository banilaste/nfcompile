package com.banilaste.nfcompile.node.variable;

/**
 * Node able to contains operations
 * @author Cl�ment
 *
 */
public interface OperationContainingNode {
	/**
	 * Function returning whether it is possible to steal the node parent
	 * @return parent stealable boolean
	 */
	public boolean canStealParent();
}
