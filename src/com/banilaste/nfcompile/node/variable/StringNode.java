package com.banilaste.nfcompile.node.variable;

import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.node.ValuableNode;
import com.banilaste.nfcompile.node.declaration.ObjectKeysNode;
import com.banilaste.nfcompile.node.declaration.VariableDeclareNode;
import com.banilaste.nfcompile.node.operation.FunctionNode;
import com.banilaste.nfcompile.type.Variable;
import com.banilaste.nfcompile.type.builtin.StringType;

/**
 * Node associated with a code string (example: node variable,
 * algorithm name...)
 * @author Cl�ment
 *
 */
public class StringNode extends ValuableNode {
	private String content;
	
	public StringNode(String content) {
		this.content = content.trim();
	}

	@Override
	public void check(AlgorithmContext ctx) throws NF4Exception {
		super.check(ctx);

		returnType = null;
		
		// Variable declare node -> no check to perform
		if (this.getParent() instanceof VariableDeclareNode || this.getParent() instanceof ObjectKeysNode) {
			return;
		}

		// Otherwise, a string shall not have children
		if (this.children().size() == 0) {
			// Object keys node + function node first child only
			if ((this.getParent() instanceof ObjectGetNode && this.getParent().child(0) != this) ||
					(this.getParent() instanceof FunctionNode && this.getParent().child(0) == this)) {
				// No type nor variable existence check
				return;
			}

			// Otherwise save type
			if (ctx.variableExists(this.content)) {
				returnType = ctx.getVariableType(this.getContent());
				return;
			}
		}
		
		throw new NF4Exception(this);
	}

	public String getContent() {
		return content;
	}

	public String toString(int level) {
		return toString(level, this.content);
	}

	@Override
	public Variable simule(AlgorithmInstance instance) throws NF4Exception {
		// If the parent require the string, not the variable
		if (this.getParent() instanceof ObjectGetNode && this.getParent().child(0) != this) {
			return new Variable(StringType.TYPE, content);
		}
		
		// Otherwise we fetch variable
		return instance.getVariable(content);
	}
}