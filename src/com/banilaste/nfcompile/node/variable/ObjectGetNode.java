package com.banilaste.nfcompile.node.variable;

import java.util.HashMap;

import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.exception.InvalidTypeException;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.node.ValuableNode;
import com.banilaste.nfcompile.type.ObjectType;
import com.banilaste.nfcompile.type.Variable;

public class ObjectGetNode extends VariableTravelNode {
	
	@Override
	public void check(AlgorithmContext ctx) throws NF4Exception {
		super.check(ctx);
		
		// A valuable parent node, 
		if (this.child(0) instanceof ValuableNode) {
			ValuableNode source = (ValuableNode) this.child(0);

			// With composed type and string node child
			if (source.getType() instanceof ObjectType &&
					this.child(1) instanceof StringNode) {

				ObjectType type = (ObjectType) source.getType();
				StringNode key = (StringNode) this.child(1);

				if (type.getKeys().containsKey(key.getContent())) {
					// We save return type :)
					returnType = type.getKeys().get(key.getContent());
					return;
				}

			}
		}		
		
		throw new NF4Exception(this);
	}


	@SuppressWarnings("unchecked")
	@Override
	public Variable operate(Variable var1, Variable var2, AlgorithmContext ctx) throws NF4Exception {
		if (var1.getType() instanceof ObjectType) {
			return ((HashMap<String, Variable>) var1.getValue()).get(var2.getValue());
		}

		throw new InvalidTypeException(this, var1.getType());
	}

}