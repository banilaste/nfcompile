package com.banilaste.nfcompile.node.variable;

import com.banilaste.nfcompile.node.operation.OperationNode;

/**
 * Node used to go from a variable to another
 * @author Cl�ment
 *
 */
public abstract class VariableTravelNode extends OperationNode {
	
	@Override
	public int getPriority() {
		return 150;
	}
}
