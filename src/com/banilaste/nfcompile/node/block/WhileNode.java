package com.banilaste.nfcompile.node.block;

import com.banilaste.nfcompile.NodeBind;
import com.banilaste.nfcompile.code.LinearNodeProvider;
import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.exception.UnexpectedException;
import com.banilaste.nfcompile.node.SeparatorNode;
import com.banilaste.nfcompile.type.Variable;
import com.banilaste.nfcompile.type.builtin.BooleanType;

public class WhileNode extends ConditionalNode {

	@Override
	public SeparatorNode parse(LinearNodeProvider provider, Program program) throws NF4Exception {
		SeparatorNode sep = super.parse(provider, program);
		
		if (sep.getBind() == NodeBind.WHILE_END) {
			return SeparatorNode.LINE_END;
		}
		
		throw new UnexpectedException(sep);
	}

	@Override
	public Variable simule(AlgorithmInstance instance) throws NF4Exception {
		while(super.simule(instance) == BooleanType.TRUE) {}
		
		return null;
	}
	
}
