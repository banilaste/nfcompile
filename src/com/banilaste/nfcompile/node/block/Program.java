package com.banilaste.nfcompile.node.block;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.LinkedList;
import com.banilaste.nfcompile.code.LinearNodeProvider;
import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.context.builtin.ReadAlgorithm;
import com.banilaste.nfcompile.context.builtin.StreamReader;
import com.banilaste.nfcompile.context.builtin.WriteAlgorithm;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.exception.RedefiningVariableException;
import com.banilaste.nfcompile.exception.UnexpectedException;
import com.banilaste.nfcompile.node.AlgorithmNode;
import com.banilaste.nfcompile.node.Node;
import com.banilaste.nfcompile.node.SeparatorNode;
import com.banilaste.nfcompile.node.SubAlgorithmNode;
import com.banilaste.nfcompile.node.declaration.TypeDeclareNode;
import com.banilaste.nfcompile.node.variable.StringNode;
import com.banilaste.nfcompile.type.Type;
import com.banilaste.nfcompile.type.Variable;
import com.banilaste.nfcompile.type.builtin.ArrayType;
import com.banilaste.nfcompile.type.builtin.BooleanType;
import com.banilaste.nfcompile.type.builtin.CharacterType;
import com.banilaste.nfcompile.type.builtin.FileType;
import com.banilaste.nfcompile.type.builtin.FloatType;
import com.banilaste.nfcompile.type.builtin.IntegerType;
import com.banilaste.nfcompile.type.builtin.StringType;

/**
 * Compiled program, containing constants and functions
 * @author Clement
 *
 */
public class Program extends BlockNode {
	private static Program instance;
	
	private HashMap<String, Type> types;
	private HashMap<String, SubAlgorithmNode> algorithms;
	private AlgorithmNode main;
	
	private InputStream defaultInputStream;
	private PrintStream defaultOutputStream;
	
	private Program() {
		
		this.types = new HashMap<String, Type>();
		this.algorithms = new HashMap<>();

		// Add basic types
		this.addType(BooleanType.TYPE);
		this.addType(IntegerType.TYPE);
		this.addType(FloatType.TYPE);
		this.addType(StringType.TYPE);
		this.addType(CharacterType.TYPE);
		
		// And built-in functions
		try {
			this.addFunction(new ReadAlgorithm(this));
			this.addFunction(new WriteAlgorithm(this));
		} catch(NF4Exception e) {
			System.err.println("Unable to add read and write functions..?");
		}
		
		// Default streams
		this.setDefaultOutputStream(System.out);
		this.setDefaultInputStream(System.in);
	}
	
	@Override
	public SeparatorNode parse(LinearNodeProvider provider, Program program) throws NF4Exception {
		// A program should not be beside another
		if (program != null) {
			throw new NF4Exception(program, "a program cannot contains another");
		}
		
		// We parse algorithms beside (or we assume they are)
		return super.parse(provider, this);
	}
	
	public SeparatorNode parseTypes(LinearNodeProvider provider, AlgorithmContext context) throws NF4Exception {
		Node node;
		SeparatorNode sep;
		
		do {
			// Fetch the node (StringNode usually)
			node = provider.next();
			
			if (node instanceof SeparatorNode) {
				sep = (SeparatorNode) node;
				continue;
			}
			
			// Then fetch it's content (should be type declare node)
			sep = node.parse(provider, this);
			
			// Verification
			node.check(context);
			
			// Getting back the type
			if (node.children().size() == 1 && node.child(0) instanceof TypeDeclareNode && node instanceof StringNode) {
				TypeDeclareNode type = (TypeDeclareNode) node.child(0);
				StringNode string = (StringNode) node;
				
				if (!this.types.containsKey(string.getContent())) {
					this.types.put(string.getContent(), type.getType());
				} else {
					throw new NF4Exception(-1, "redefining type " + string.getContent());
				}
			} else {
				throw new UnexpectedException(node);
			}
		} while(sep == SeparatorNode.LINE_END);
		
		return sep;
	}

	public void setMain(AlgorithmNode algo) throws NF4Exception {
		if (this.main != null) {
			throw new NF4Exception(algo, "there is already a main algorithm");
		}
		
		this.main = algo;
	}
	
	@Override
	public Variable simule(AlgorithmInstance instance) throws NF4Exception {
		AlgorithmInstance inst = main.newInstance();
		inst.parameters(new LinkedList<>(), new LinkedList<>());
		
		return main.simule(inst);
	}

	public void addType(Type type) {
		// We only add non-empty named types, and not arrays
		if (!type.getName().equals("") && !(type instanceof ArrayType)) {
			types.put(type.getName(), type);
		}
	}
	
	public Type getType(String name) {
		return types.get(name);
	}
	
	public void addFunction(SubAlgorithmNode sub) throws NF4Exception {
		// Name duplication
		if (this.algorithms.containsKey(sub.getName())) {
			throw new RedefiningVariableException(sub, sub.getName());
		}
		
		this.algorithms.put(sub.getName(), sub);
	}
	
	public SubAlgorithmNode getFunction(String name) {
		return algorithms.get(name);
	}

	public InputStream getDefaultInputStream() {
		return defaultInputStream;
	}

	public void setDefaultInputStream(InputStream defaultInputStream) {
		this.defaultInputStream = defaultInputStream;
		
		FileType.CONSOLE.setValue(new StreamReader(defaultInputStream));
	}

	public PrintStream getDefaultOutputStream() {
		return defaultOutputStream;
	}

	public void setDefaultOutputStream(PrintStream defaultOutputStream) {
		this.defaultOutputStream = defaultOutputStream;
	}

	// TODO store node simulation
	public Node getSimulated() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public HashMap<String, SubAlgorithmNode> algorithms() {
		return algorithms;
	}

	/*
	 * Singleton functions
	 */
	public static Program create() {
		instance = new Program();
		
		return instance;
	}
	
	public static Program get() {
		return instance;
	}
}