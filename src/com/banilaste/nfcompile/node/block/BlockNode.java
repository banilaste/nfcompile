package com.banilaste.nfcompile.node.block;

import java.util.Iterator;

import com.banilaste.nfcompile.NodeBind;
import com.banilaste.nfcompile.code.LinearNodeProvider;
import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.node.Node;
import com.banilaste.nfcompile.node.SeparatorNode;
import com.banilaste.nfcompile.type.Variable;

/**
 *	Node containing multiple instructions
 */
public class BlockNode extends Node {
	public SeparatorNode parse(LinearNodeProvider provider, Program program) throws NF4Exception {
		SeparatorNode separator;
		
		do {
			separator = super.parse(provider, program);
		} while(separator.getBind() == NodeBind.LINE_END);

		return separator;
	}

	@Override
	public Variable simule(AlgorithmInstance instance) throws NF4Exception {
		for (Iterator<Node> iter = this.children().iterator(); iter.hasNext();) {
			iter.next().simule(instance);
		}

		return null;
	}
	
}