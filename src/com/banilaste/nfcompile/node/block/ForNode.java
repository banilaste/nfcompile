package com.banilaste.nfcompile.node.block;

import java.util.Iterator;

import com.banilaste.nfcompile.NodeBind;
import com.banilaste.nfcompile.code.LinearNodeProvider;
import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.exception.InvalidTypeException;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.exception.UnaffectableException;
import com.banilaste.nfcompile.exception.UnexpectedException;
import com.banilaste.nfcompile.node.Node;
import com.banilaste.nfcompile.node.SeparatorNode;
import com.banilaste.nfcompile.node.ValuableNode;
import com.banilaste.nfcompile.type.Variable;
import com.banilaste.nfcompile.type.builtin.IntegerType;

public class ForNode extends Node {

	@Override
	public synchronized SeparatorNode parse(LinearNodeProvider provider, Program program) throws NF4Exception {
		SeparatorNode separator;

		// First parse parameters
		do {
			separator = super.parse(provider, program);
		} while(separator.getBind() == NodeBind.FOR_SEPARATOR);

		// If for ended rightaway, no need further parsing
		if (separator.getBind() == NodeBind.FOR_END) {
			return new SeparatorNode(NodeBind.LINE_END);
		}

		// Otherwise line should be ended
		else if (separator.getBind() != NodeBind.LINE_END) {
			throw new UnexpectedException(this);
		}

		BlockNode block = new BlockNode();
		this.addChild(block);

		// Parse content using regular node function
		separator = block.parse(provider, program);

		// If for loop unclosed (should not end on end of a if of while node)
		if (separator.getBind() != NodeBind.FOR_END) {
			throw new UnexpectedException(this);
		}

		// Instructions are over for this node
		return new SeparatorNode(NodeBind.LINE_END);
	}

	@Override
	public void check(AlgorithmContext ctx) throws NF4Exception {
		super.check(ctx);

		Node testedNode;

		if (this.children().size() != 4 && this.children().size() != 5) {
			throw new UnexpectedException(this.children().getLast());
		}

		testedNode = this.children().get(0);

		// First param should be a integer variable
		if (!ValuableNode.isAffectable(testedNode)) {
			throw new UnaffectableException(testedNode);

		}

		// Following parameters should be integers
		for (int i = 0; i < this.children().size() - 1; i++) {
			// And they all should be integers (yeah not float because we aren't nice)
			if (!ValuableNode.childIsType(this, i, IntegerType.TYPE)) {
				throw new InvalidTypeException(this.child(i), ValuableNode.getType(this, i), IntegerType.TYPE);
			}
		}
	}

	@Override
	public Variable simule(AlgorithmInstance instance) throws NF4Exception {
		Iterator<Node> iter = this.children().iterator();
		Variable counter = iter.next().simule(instance);

		counter.setValue(iter.next().simule(instance).getValue());

		int max = (int) iter.next().simule(instance).getValue();
		int step;

		if (this.children().size() == 5) {
			step = (int) iter.next().simule(instance).getValue();
		} else {
			step = 1;
		}

		Node content = iter.next();

		if (step < 0) {
			for (; (int) counter.getValue() >= max; counter.setValue((int) counter.getValue() + step)) {
				content.simule(instance);
			}
		} else {
			for (; (int) counter.getValue() <= max; counter.setValue((int) counter.getValue() + step)) {
				content.simule(instance);
			}
		}

		return null;
	}


}