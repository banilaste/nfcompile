package com.banilaste.nfcompile.node.block;

import java.util.Iterator;

import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.exception.InvalidTypeException;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.node.Node;
import com.banilaste.nfcompile.node.ValuableNode;
import com.banilaste.nfcompile.type.Variable;
import com.banilaste.nfcompile.type.builtin.BooleanType;

/**
 *	Node executed upon condition is validated
 */
public class ConditionalNode extends BlockNode {

	@Override
	public void check(AlgorithmContext ctx) throws NF4Exception {
		// Regular check first
		super.check(ctx);

		// Then check if the first node return a boolean
		if (!ValuableNode.childIsType(this, 0, BooleanType.TYPE)) {
			throw new InvalidTypeException(this.child(0), ValuableNode.getType(this, 0), BooleanType.TYPE);
		}
	}

	@Override
	public Variable simule(AlgorithmInstance instance) throws NF4Exception {
		Iterator<Node> iter = this.children().iterator();
		
		if (iter.next().simule(instance).getValue().equals(BooleanType.TRUE.getValue())) {
			while (iter.hasNext()) {
				iter.next().simule(instance);
			}
			
			return BooleanType.TRUE;
		}
		
		return BooleanType.FALSE;
	}
	
	
}