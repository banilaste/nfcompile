package com.banilaste.nfcompile.node.block;

import java.util.Iterator;

import com.banilaste.nfcompile.NodeBind;
import com.banilaste.nfcompile.code.LinearNodeProvider;
import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.exception.UnexpectedException;
import com.banilaste.nfcompile.node.Node;
import com.banilaste.nfcompile.node.SeparatorNode;
import com.banilaste.nfcompile.type.Variable;
import com.banilaste.nfcompile.type.builtin.BooleanType;

/**
 *	Node containing if-elseif-else structure
 */
public class IfNode extends Node {
	@Override
	public SeparatorNode parse(LinearNodeProvider provider, Program program) throws NF4Exception {
		SeparatorNode separator;
		Node subNode;
		
		// We first parse if and else if blocks (both require a condition)
		do {
			subNode = new ConditionalNode ();
			separator = subNode.parse(provider, null);
			
			this.addChild(subNode);
		} while(separator.getBind() == NodeBind.ELSE_IF);
		
		// Then if there is a ELSE node
		if (separator.getBind() == NodeBind.ELSE) {
			// It shall be parsed too
			subNode = new BlockNode();
			separator = subNode.parse(provider, null);
			
			this.addChild(subNode);
		}
		
		// If the separator node does not mark the
		// end of the if-else structure
		if (separator.getBind() != NodeBind.IF_END) {
			throw new UnexpectedException(separator);
		}
		
		return new SeparatorNode(NodeBind.LINE_END);
	}

	@Override
	public Variable simule(AlgorithmInstance instance) throws NF4Exception {
		Iterator<Node> iter = this.children().iterator();
		
		while(iter.hasNext() && iter.next().simule(instance) == BooleanType.FALSE) {}
		
		return null;
	}
}