package com.banilaste.nfcompile.trace;

import java.util.Observable;
import java.util.Observer;
import java.util.Stack;

import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.node.SubAlgorithmNode;
import com.banilaste.nfcompile.node.block.Program;

public class ProgramObserver implements Observer {
	private Stack<VariableObserver> variableObservers; 
	
	public ProgramObserver(Program program) {
		variableObservers = new Stack<>();
		
		program.algorithms().forEach((name, algorithm) -> {
			algorithm.addObserver(this);
		});
	}

	@Override
	public void update(Observable observable, Object argument) {
		if (observable instanceof SubAlgorithmNode) {
			// If the given argument is an instance of an algorithm
			if (argument instanceof AlgorithmInstance) {
				// We create a new observer for the given instance
				variableObservers.push(new VariableObserver((AlgorithmInstance) argument));
			} else {
				// We pop the observer and notify current one that a algorithm has been run
				VariableObserver observer = variableObservers.pop();

				observer.terminate();
			}
		}
	}
}
