package com.banilaste.nfcompile.trace;

import java.util.HashMap;

import com.banilaste.nfcompile.type.Variable;

public class LineActions {
	private HashMap<String, Object> values;
	private int line;
	
	public LineActions(int line) {
		this.line = line;
		this.values = new HashMap<String, Object>();
	}
	
	public void add(String name, Variable var) {
		values.put(name, var);
	}

	public HashMap<String, Object> getValues() {
		return values;
	}

	public int getLine() {
		return line;
	}
}
