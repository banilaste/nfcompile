package com.banilaste.nfcompile.trace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.type.Variable;

public class VariableObserver extends Observable implements Observer {
	private HashMap<Variable, String> names;
	private ArrayList<LineActions> actions;
	private AlgorithmInstance instance;
	private LineActions current;
	
	public VariableObserver(AlgorithmInstance instance) {
		this.names = new HashMap<>();
		this.instance = instance;
		this.actions = new ArrayList<>();
		
		instance.variables().forEach((name, variable) -> {
			variable.addObserver(this);
			
			// Store name
			names.put(variable, name);
		});
		
		current = new LineActions(instance.getLine());
	}

	public void terminate() {
		actions.add(current);
	}

	@Override
	public void update(Observable o, Object arg) {
		int line = instance.getLine();
		
		// If line is finished, 
		if (line != current.getLine()) {
			actions.add(current);
			current = new LineActions(line);
			
			// Notify observers that a line has been changed
			this.notifyObservers();
		}
		
		Variable var = (Variable) o;
		
		
		current.add(names.get(var), var);
	}
}
