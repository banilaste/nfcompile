package com.banilaste.nfcompile;

import com.banilaste.nfcompile.code.LinearNodeProvider;
import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.node.SeparatorNode;
import com.banilaste.nfcompile.node.block.Program;

public interface Parsable {
	/**
	 * Parse content from provider and program
	 * @param provider content provider
	 * @param program root program
	 * @return separator unused by the component
	 * @throws NF4Exception exception throw on parsing invalid content
	 */
	public SeparatorNode parse(LinearNodeProvider provider, Program program) throws NF4Exception;
	public void check(AlgorithmContext ctx) throws NF4Exception;
}
