package com.banilaste.nfcompile.context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.banilaste.nfcompile.NodeBind;
import com.banilaste.nfcompile.code.LinearNodeProvider;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.node.SeparatorNode;
import com.banilaste.nfcompile.node.block.Program;
import com.banilaste.nfcompile.node.declaration.ObjectKeysNode;
import com.banilaste.nfcompile.node.declaration.VariableDeclareNode;
import com.banilaste.nfcompile.type.Type;
import com.banilaste.nfcompile.type.builtin.FileType;

/**
 * Manage context of a single algorithm or function
 * @author Clement
 *
 */
public class AlgorithmContext {
	public final static String CONSOLE_INPUT = "clavier";
	
	private HashMap<String, Type> variables;
	private Program program;
	protected ObjectKeysNode inputParameters, outputParameters;
	private int startingLine;
	
	public AlgorithmContext(Program program) {
		this.variables = new HashMap<String, Type>();
		this.program = program;
		
		// Add base variable
		this.variables.put(CONSOLE_INPUT, FileType.TYPE);
	}

	/**
	 * Parse variables of an algorithm or a sub-algorithm
	 * @return separator node
	 * @throws NF4Exception 
	 */
	public SeparatorNode parseVariables(LinearNodeProvider provider, Program program) throws NF4Exception {
		VariableDeclareNode node;
		SeparatorNode separator;
		
		do {				
			// Then get regular variable
			node = new VariableDeclareNode();
			separator = node.parse(provider, program);
				
			// We check now because we won't store the nodes
			node.check(this);
				
			String names[] = node.getNames();

			for (int i = 0; i < names.length; i++) {
				this.addVariable(names[i], node.getType(this));
			}		
			
		} while(separator.getBind() == NodeBind.LINE_END);
		
		return separator;
	}

	/**
	 * Return type of a variable described by it's name
	 * @param string name of the variable
	 * @return type of the given variable, null if the variable does not exists
	 */
	public Type getVariableType(String string) {
		if (variables.containsKey(string)) {
			return variables.get(string);
			
		} else if (inputParameters.getKeys().containsKey(string)) {
			return inputParameters.getKeys().get(string);
			
		} else if (outputParameters.getKeys().containsKey(string)) {
			return outputParameters.getKeys().get(string);
		} 
		return null;
	}

	public void addVariable(String name, Type type) throws NF4Exception {
		if (variableExists(name)) {
			throw new NF4Exception(-1, "variable redefinition");
		}

		variables.put(name, type);
	}

	public Program getProgram() {
		return program;
	}

	public boolean variableExists(String content) {
		if (variables.containsKey(content)) {
			return true;
		} else if (inputParameters != null && inputParameters.getKeys().containsKey(content)) {
			return true;
		}

		return outputParameters != null && outputParameters.getKeys().containsKey(content);
	}

	public HashMap<String, Type> getVariables() {
		return variables;
	}

	public void checkInputParameters(ArrayList<Type> types) throws NF4Exception {
		checkParameters(types, getInputParameters().getOrderedNames(), getInputParameters().getKeys());
	}
	
	public void checkOutputParameters(ArrayList<Type> types) throws NF4Exception {
		// If only one output arg, we can omit a return arg
		if (getOutputParameters().getOrderedNames().size() == 1 && types.size() == 0) {
			return;
		}
		
		checkParameters(types, getOutputParameters().getOrderedNames(), getOutputParameters().getKeys());
	}
	
	private void checkParameters(ArrayList<Type> given, ArrayList<String> names, HashMap<String, Type> bind) throws NF4Exception {
		if (given.size() != names.size()) {
			throw new NF4Exception(-1, "not enough parameters");
		}
		
		Iterator<Type> i1 = given.iterator();
		Iterator<String> i2 = names.iterator();
		
		// We compare every type
		while (i1.hasNext()) {
			// If one is not compatible
			if (!i1.next().castableTo(bind.get(i2.next()))) {
				throw new NF4Exception(-1, "incompatible types");
			}
		}
	}

	/**
	 * Function called when no modification to the context is done
	 */
	public void parsingDone() {
		if (outputParameters == null) {
			outputParameters = new ObjectKeysNode();
			outputParameters.empty();
		}
		
		if (inputParameters == null) {
			inputParameters = new ObjectKeysNode();
			inputParameters.empty();
		}
	}
	
	public ObjectKeysNode getInputParameters() {
		return inputParameters;
	}

	public ObjectKeysNode getOutputParameters() {
		return outputParameters;
	}
	
	public void setInputParameters(ObjectKeysNode keys) throws NF4Exception {
		if (this.inputParameters != null) {
			throw new NF4Exception(-1, "input parameters already defined");
		}
	
		this.inputParameters = keys;
	}
	
	public void setOutputParameters(ObjectKeysNode keys) throws NF4Exception {
		if (this.outputParameters != null) {
			throw new NF4Exception(-1, "output parameters already defined");
		}
		
		this.outputParameters = keys;
	}

	public int getStartingLine() {
		return startingLine;
	}

	public void setStartingLine(int startingLine) {
		this.startingLine = startingLine;
	}

}