package com.banilaste.nfcompile.context.builtin;

import java.util.ArrayList;
import java.util.InputMismatchException;

import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.node.Node;
import com.banilaste.nfcompile.node.block.Program;
import com.banilaste.nfcompile.type.Type;
import com.banilaste.nfcompile.type.Variable;
import com.banilaste.nfcompile.type.builtin.CharacterType;
import com.banilaste.nfcompile.type.builtin.FileType;
import com.banilaste.nfcompile.type.builtin.FloatType;
import com.banilaste.nfcompile.type.builtin.IntegerType;

public class ReadAlgorithm extends BuiltInAlgorithm {
	private final static String READ_FUNCTION = "Lire";
	
	public ReadAlgorithm (Program program) {
		super(program, new BuiltInContext(program) {
			private Type readType;
			
			@Override
			public void checkOutputParameters(ArrayList<Type> types) throws NF4Exception {
				if (types.size() != 1) {
					// TODO manage multiple
					throw new NF4Exception(-1, "only one parameter is accepted for " + READ_FUNCTION);
				}
				
				readType = types.get(0);
				
				if (readType instanceof IntegerType || readType instanceof FloatType ||
						readType instanceof CharacterType) {
					
					return;
				}
				
				throw new NF4Exception(-1, "cannot read a data of type " + readType);
			}
			
			@Override
			public void checkInputParameters(ArrayList<Type> types) throws NF4Exception {
				// Should input a file
				if (types.size() != 1 || types.get(0) != FileType.TYPE) {
					throw new NF4Exception((Node) null);
				}
			}
		}, READ_FUNCTION);
		
		// Add required child
		this.getContext().getInputParameters().getKeys().put("file", FileType.TYPE);
		this.getContext().getInputParameters().getOrderedNames().add("file");
	}
	
	@Override
	public Variable simule(AlgorithmInstance instance) throws NF4Exception {
		Variable file = instance.getVariable("file");
		StreamReader sc = (StreamReader) file.getValue();
		Type returnType = instance.getOutputs().get(0).getType();
		
		Object value = null;

		try {
			if (returnType == IntegerType.TYPE) {
				value = sc.nextInt();
			} else if (returnType == FloatType.TYPE) {
				value = sc.nextFloat();
			} else if (returnType == CharacterType.TYPE) {
				value = sc.nextChar();
			}
		} catch (InputMismatchException e) {
			e.printStackTrace();
			throw new NF4Exception(this);
		}
		
		// Apply directly on return variable
		instance.getOutputs().get(0).setValue(value);
		
		// No value can be fetched directly
		return null;
	}
}
