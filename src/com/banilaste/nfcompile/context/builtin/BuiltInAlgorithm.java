package com.banilaste.nfcompile.context.builtin;

import com.banilaste.nfcompile.code.LinearNodeProvider;
import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.node.SeparatorNode;
import com.banilaste.nfcompile.node.SubAlgorithmNode;
import com.banilaste.nfcompile.node.block.Program;
import com.banilaste.nfcompile.type.Variable;

public class BuiltInAlgorithm extends SubAlgorithmNode {
	public BuiltInAlgorithm(Program program, BuiltInContext context, String name) {
		this.context = context;
		this.name = name;
	}

	@Override
	public SeparatorNode parse(LinearNodeProvider provider, Program program) throws NF4Exception {
		return null;
	}

	@Override
	public void check(AlgorithmContext ctx) throws NF4Exception {/* builtin : no check */}

	@Override
	public Variable simule(AlgorithmInstance instance) throws NF4Exception {
		return null;
	}


}
