package com.banilaste.nfcompile.context.builtin;

import java.io.IOException;
import java.io.InputStream;

import com.banilaste.nfcompile.exception.NF4Exception;

public class StreamReader {
	private InputStream stream;
	private boolean wasNumber;
	private char overflow = 0; // overflow for checking after numbers
	
	public StreamReader(InputStream stream) {
		this.stream = stream;
		
		this.wasNumber = false;
	}
	
	public int nextInt() throws NF4Exception {
		String string = "";
		char read = wasNumber ? overflow : next();
		
		
		while(Character.isWhitespace(read)) {
			read = next();
		}

		if (read == '-' || read == '+') {
			string += read;
			read = next();
		}
		
		do {
			string += read;
			
			read = next();
		} while(Character.isDigit(read));

		this.wasNumber = true;
		this.overflow = read;
		
		if (!Character.isWhitespace(overflow)) {
			throw new NF4Exception(-1, "unexpected character : " + overflow);
		}
		
		return Integer.parseInt(string);
	}
	
	public float nextFloat() throws NF4Exception {
		String string = "";
		char read = wasNumber ? overflow : next();

		
		while(Character.isWhitespace(read)) {
			read = next();
		}

		if (read == '-' || read == '+') {
			string += read;
			read = next();
		}
		
		while(Character.isDigit(read) || read == '.') {
			string += read;
			
			read = next();
		}
		
		this.wasNumber = true;
		this.overflow = read;
		
		if (!Character.isWhitespace(overflow)) {
			throw new NF4Exception(-1, "unexpected character : " + overflow);
		}
		
		return Float.parseFloat(string);
	}
	
	public char nextChar() {
		char content = wasNumber ? overflow : next();
		
		if (wasNumber) {
			while(Character.isWhitespace(content)) {
				content = next();
			}
			
			wasNumber = false;
		}
		
		if (content == '\n') {
			return '\0';
		}
		
		return content;
	}
	
	private char next() {
		try {
			return (char) stream.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return '\0';
	}
}
