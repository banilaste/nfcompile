package com.banilaste.nfcompile.context.builtin;

import java.util.ArrayList;
import java.util.Iterator;

import com.banilaste.nfcompile.context.AlgorithmInstance;
import com.banilaste.nfcompile.exception.InvalidTypeException;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.node.block.Program;
import com.banilaste.nfcompile.type.Type;
import com.banilaste.nfcompile.type.Variable;
import com.banilaste.nfcompile.type.builtin.CharacterType;
import com.banilaste.nfcompile.type.builtin.FloatType;
import com.banilaste.nfcompile.type.builtin.IntegerType;
import com.banilaste.nfcompile.type.builtin.StringType;

public class WriteAlgorithm extends BuiltInAlgorithm {
	private final static String WRITE_FUNCTION = "Ecrire";
	
	public WriteAlgorithm(Program program) {
		super(program, new BuiltInContext(program) {
			
			@Override
			public void checkOutputParameters(ArrayList<Type> types) throws NF4Exception {
				// No need to compare with existing types, write function accept them all
			}
			
			@Override
			public void checkInputParameters(ArrayList<Type> types) throws NF4Exception {
				for (Iterator<Type> iter = types.iterator(); iter.hasNext();) {
					Type type = iter.next();
					
					if (type != CharacterType.TYPE && type != IntegerType.TYPE &&
							type != FloatType.TYPE && type != StringType.TYPE) {
						
						throw new InvalidTypeException(Program.get().getSimulated(), type);
					}
				}
			}
		}, WRITE_FUNCTION);
	}
	
	@Override
	public Variable simule(AlgorithmInstance instance) {
		Iterator<Variable> vars = instance.getInputs().iterator();
		StringBuffer buffer = new StringBuffer();
 
		while(vars.hasNext()) {
			buffer.append(vars.next().getValue().toString());
		}
		
		// Print to default output
		instance.getProgram().getDefaultOutputStream().print(buffer.toString().replace("\\n", "\n"));
		return null;
	}
}