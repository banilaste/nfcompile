package com.banilaste.nfcompile.context.builtin;

import java.util.ArrayList;

import com.banilaste.nfcompile.context.AlgorithmContext;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.node.block.Program;
import com.banilaste.nfcompile.node.declaration.ObjectKeysNode;
import com.banilaste.nfcompile.type.Type;

public abstract class BuiltInContext extends AlgorithmContext {
	public BuiltInContext(Program program) {
		super(program);
		
		this.inputParameters = new ObjectKeysNode();
		this.outputParameters = new ObjectKeysNode();
		
		this.inputParameters.empty();
		this.outputParameters.empty();
	}

	@Override
	public abstract void checkOutputParameters(ArrayList<Type> types) throws NF4Exception;

	@Override
	public abstract void checkInputParameters(ArrayList<Type> types) throws NF4Exception;
	
	
}
