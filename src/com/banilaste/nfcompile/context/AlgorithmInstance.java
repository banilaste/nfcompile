package com.banilaste.nfcompile.context;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import com.banilaste.nfcompile.node.block.Program;
import com.banilaste.nfcompile.type.Type;
import com.banilaste.nfcompile.type.Variable;
import com.banilaste.nfcompile.type.builtin.FileType;

/**
 * Instance of a function, store variables
 * @author Cl�ment
 *
 */
public class AlgorithmInstance {
	private AlgorithmContext context;
	private HashMap<String, Variable> variables;
	private LinkedList<Variable> outputs, inputs;
	
	public AlgorithmInstance(AlgorithmContext context) {
		this.context = context;
		this.variables = new HashMap<>();
		
		// Initialize every variables
		for (Iterator<String> iter = context.getVariables().keySet().iterator(); iter.hasNext();) {
			String key = iter.next();
			
			variables.put(key, new Variable(context.getVariables().get(key)).init());
		}
		
		// Init console input
		this.variables.put(AlgorithmContext.CONSOLE_INPUT, FileType.CONSOLE);
	}
	
	public AlgorithmContext getContext() {
		return context;
	}
	
	public Program getProgram() {
		return context.getProgram();
	}
	
	public Variable getVariable(String name) {
		return variables.get(name);
	}

	/**
	 * Define parameters of current instance
	 * @param inputs input variables
	 * @param outputs output variables
	 */
	public void parameters(LinkedList<Variable> inputs, LinkedList<Variable> outputs) {
		Iterator<String> names = context.getInputParameters().getOrderedNames().iterator();
		Iterator<Variable> input = inputs.iterator();
		
		this.inputs = inputs;
		
		// Copy data to input variables
		while (names.hasNext()) {
			String name = names.next();
			
			this.variables.put(name, input.next().copy());
		}
		
		// Store output for execution
		this.outputs = outputs;
		
		// But create variables aswell
		names = context.getOutputParameters().getOrderedNames().iterator();
		HashMap<String, Type> variables = context.getOutputParameters().getKeys();
		
		while (names.hasNext()) {
			String name = names.next();
			this.variables.put(name, new Variable(variables.get(name)).init());
		}
	}
	
	/**
	 * Copy return variables a the end of the function
	 * @return variable return by the call if any
	 */
	public Variable applyOutput() {
		Iterator<Variable> outputs = this.outputs.iterator();
		Iterator<String> names = context.getOutputParameters().getOrderedNames().iterator();

		while(outputs.hasNext()) {
			outputs.next().setValue(variables.get(names.next()).getValue());
		}
		
		// We return last unparsed argument
		if (this.outputs.size() == 0 && names.hasNext()) {
			return variables.get(names.next());
		}
		
		return null;
	}

	/**
	 * Get line relative to algorithm line
	 * @return
	 */
	public int getLine() {
		return getProgram().getSimulated().getLine() - context.getStartingLine();
	}
	
	public LinkedList<Variable> getInputs() {
		// Used by write
		return inputs;
	}

	public LinkedList<Variable> getOutputs() {
		// Used by read
		return outputs;
	}

	public HashMap<String, Variable> variables() {
		return variables;
	}
}
