package com.banilaste.nfcompile;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.regex.Pattern;

import com.banilaste.nfcompile.code.RegexUtil;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.node.*;
import com.banilaste.nfcompile.node.block.*;
import com.banilaste.nfcompile.node.operation.*;
import com.banilaste.nfcompile.node.operation.comparison.*;
import com.banilaste.nfcompile.node.declaration.*;
import com.banilaste.nfcompile.node.variable.*;

public enum NodeBind {
	INPUT_PARAMETER(RegexUtil.keyword("PE :"), null),
	OUTPUT_PARAMETER(RegexUtil.keyword("PS :"), null),
	ARRAY_DECLARE_SEPARATOR("\\.\\.\\.?", null),
	AFFECT("<-", AffectNode.class),
	SUBALGORITHM(RegexUtil.keyword("sous algorithme"), SubAlgorithmNode.class),
	ALGORITHM(RegexUtil.keyword("algorithme"), AlgorithmNode.class),
	ADDITION("\\+", AdditionNode.class),
	SUBSTRACTION("-", SubstractionNode.class),
	MULTIPLICATION("\\*", MultiplicationNode.class),
	DIVISION("/", DivisionNode.class),
	WHILE_END(RegexUtil.keyword("fin ((tant que)|(tq))"), null),
	WHILE(RegexUtil.keyword("tant que"), WhileNode.class),
	FOR_END(RegexUtil.keyword("fin pour"), null),
	FOR(RegexUtil.keyword("pour"), ForNode.class),
	FOR_SEPARATOR(RegexUtil.keyword("((de)|(�)|(par pas de))"), null),
	ELSE_IF(RegexUtil.keyword("sinon si"), null),
	IF_END(RegexUtil.keyword("fin si"), null),
	IF(RegexUtil.keyword("si"), IfNode.class),
	ELSE(RegexUtil.keyword("sinon"), null),
	END(RegexUtil.keyword("fin"), null),
	OBJECT_TYPE(RegexUtil.keyword(": article \\(", ""), TypeDeclareNode.class),
	FUNCTION_PARENTHESIS_START(RegexUtil.keyword(" \\(", RegexUtil.VARIABLE, ""), FunctionNode.class),
	PARENTHESIS_START("\\(", ParenthesisNode.class),
	PARENTHESIS_END("\\)", null),
	OR(RegexUtil.keyword("ou", "[^a-zA-Z0-9]"), OrNode.class),
	AND(RegexUtil.keyword("et", "[^a-zA-Z0-9]"), AndNode.class),
	ARRAY_BRACKETS("\\[", ArrayGetNode.class),
	ARRAY_BRACKETS_END("\\]", null),
	OBJECT_DOT("\\.", ObjectGetNode.class),
	LINE_END,
	VARIABLE_TYPE(":", TypeNode.class),
	TYPE_ARRAY(RegexUtil.keyword("tableau"), null),
	VARIABLE_SEPARATOR(",", null),
	INSTRUCTIONS_START(RegexUtil.keyword("instructions"), null),
	TYPES_START(RegexUtil.keyword("types"), null),
	VARIABLES_START(RegexUtil.keyword("variables"), null),
	FILE_END,
	DIFFERENT("!=", DifferentNode.class),
	FUNCTION_IO_SPLIT("!", null),
	LESSER_EQUALS("<=", LesserOrEqualsNode.class),
	GREATER_EQUALS(">=", GreaterOrEqualsNode.class),
	LESSER("<", LesserNode.class),
	GREATER(">", GreaterNode.class),
	EQUALS("=", EqualsNode.class);
	
	// TODO enum...
	
	
	private Pattern descriptor;
	private Constructor<? extends Node> constructor;
	
	private NodeBind(Pattern desc, Class<? extends Node> nodeClass) {
		this.descriptor = desc;
		
		if (nodeClass != null) {
			try {
				constructor = nodeClass.getConstructor();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Create a binding between regular expression and it's matching node
	 * @param descriptor regular expression in string, or null if it can't be parsed
	 * @param nodeClass class of the node being created, or null if no node shall be created
	 */
	private NodeBind(String descriptor, Class<? extends Node> nodeClass) {
		this(descriptor == null ? null : Pattern.compile(descriptor), nodeClass);
	}
	
	private NodeBind() {
		this((Pattern) null, null);
	}
	
	public Node create() {
		// If no constructor given (empty initial class)
		if (this.constructor == null) {
			// A separator is sent back to inform there is no node for this data
			return new SeparatorNode(this);
		}
		
		try {
			return constructor.newInstance(new Object[0]);
		} catch (InvocationTargetException exception) {
			// Exception while creating node
			exception.getCause().printStackTrace();
		}  catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Return node associated with given character
	 * @param key character to lookup
	 * @return
	 * @throws NF4Exception 
	 */
	@Deprecated
	public static Node get(char key) throws NF4Exception {
		return null;
	}
	

	public Pattern getDescriptor() {
		return descriptor;
	}

	@Deprecated
	public char getKey() {
		return 0;
	}
}