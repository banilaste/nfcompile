package com.banilaste.nfcompile.code;

import com.banilaste.nfcompile.exception.UnexpectedException;
import com.banilaste.nfcompile.node.Node;

public class CodeLine {
	private NodeString content;
	private int lineNumber;
	
	public CodeLine(NodeString content, int line) {
		this.content = content;
		this.lineNumber = line;
	}
	
	/**
	 * Return next node contained in the line
	 * @return node extracted
	 * @throws UnexpectedException 
	 */
	public Node next() throws UnexpectedException {
		NodeStringPart part = content.getParts().removeFirst();
		
		if (part.isString()) {
			throw new UnexpectedException(this.lineNumber, part.getContent());
		}

		return part.getNode();
	}
	
	public boolean canExtract() {
		return content.getParts().size() > 0;
	}
	
	public String toString() {
		return content.toString();
	}

	public int getLineNumber() {
		return lineNumber;
	}
}
