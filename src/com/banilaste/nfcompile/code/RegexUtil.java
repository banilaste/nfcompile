package com.banilaste.nfcompile.code;

import java.util.regex.Pattern;

public abstract class RegexUtil {
	public final static String WHITESPACES = "\\s+";
	public final static String VARIABLE = "[a-zA-Z0-9_]";
	public final static String NUMBER = "[0-9]";
	
	public static Pattern keyword(String keyword) {
		return keyword(keyword, "\\s+");
	}
	
	public static Pattern keyword(String keyword, String bound) {
		return keyword(keyword, bound, bound);
	}
	
	public static Pattern keyword(String keyword, String start, String end) {
		StringBuffer bf = new StringBuffer();
		
		// Content before keyword (to match not to replace)
		bf.append("(?i)(?<start>");
		bf.append(start);
		bf.append(")");
		
		// Content matched to be replaced (whitespaces are replaced in a more general way)
		bf.append(keyword.replaceAll(" ", "\\\\s*"));
		
		// Content after keyword
		bf.append("(?<end>");
		bf.append(end);
		bf.append(")");
		
		return Pattern.compile(bf.toString());
	}
}