package com.banilaste.nfcompile.code;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.banilaste.nfcompile.code.NodeString.NodeStringMatcher;
import com.banilaste.nfcompile.node.Node;

/**
 * Class containing a part of the code
 *
 */
public class NodeStringPart {
	private Node node = null;
	private String content;

	public NodeStringPart(String content) {
		this.content = content;
	}

	public NodeStringPart(Node node) {
		this.node = node;
	}

	/**
	 * Replace expression by string or node
	 * 
	 * Regex may contain <b>start</b> or/and <b>end</b> groups, which contains content that
	 * should not be replaced.
	 * 
	 * Example:
	 * <pre>
	 * {@code
	 * StringPart part = new StringPart("i am a red boat");
	 * part.replace(Pattern.compile("(?<start>a )[a-z]+(?<end> boat)"), replacement, parts);
	 * }
	 * </pre>
	 * 
	 * This code should only replace "red" by the part given by the replacement, despite matching
	 * "a" and "boat".
	 * 
	 * @param regex regular expression
	 * @param callback callback giving the right replacement for each match
	 * @param parts parts of the new node string being built
	 * 
	 * 
	 */
	public void replace(Pattern regex, NodeStringMatcher replacement, LinkedList<NodeStringPart> parts) {
		// If the part is already a node, it cannot be parsed
		if (!isString()) {
			parts.add(this);
			return;
		}

		Matcher matcher = regex.matcher(" " + content + " ");
		StringBuffer bf = new StringBuffer();
		String start = "", end = "";
		NodeStringPart part;

		while(matcher.find()) {
			// Fetch ending and starting groups (not used in replacement but matched in regex)
			try {
				start = matcher.group("start");
			} catch(IllegalArgumentException e) {
				start = "";
			}
			
			try {
				end = matcher.group("end");
			} catch(IllegalArgumentException e) {
				end = "";
			}
			
			// Check what is the issue (WHILENODE ADDED)
			if (start == null) {
				start = "";
			}
			
			if (end == null) {
				end = "";
			}
			
			// Fetching part given by the callback
			part = replacement.replaceBy(matcher);

			// Get content before match
			matcher.appendReplacement(bf, "");
			bf.append(start);

			// If the replacement is a string
			if (part.isString()) {
				// We can add the replacement inside the same part
				bf.append(part.getContent());
			}

			// Otherwise we should make several parts
			else {
				// First what was before node
				parts.add(new NodeStringPart(bf.toString()));

				// Then add the replacement part
				parts.add(part);

				// And we reset the buffer
				bf = new StringBuffer();
			}

			// We add the end of the string to the buffer
			bf.append(end);
		}
		
		// Add the remainder
		matcher.appendTail(bf);

		// We reuse this part to save memory
		this.content = bf.toString();
		parts.add(this);
	}

	boolean isString() {
		return node == null;
	}
	
	public String getContent() {
		return content;
	}
	
	/**
	 * Return string contained in part or empty string if it contains a node
	 */
	public String toString() {
		if (isString()) {
			return content.replaceAll("\n", "\\\\n");
		} else {
			return node.toString();
		}
	}

	/**
	 * Remove whitespaces
	 */
	public void clean() {
		// No need to clean nodes
		if (!isString()) {
			return;
		}
		
		this.content = this.content.replaceAll("\r|\\s", "");
	}

	public Node getNode() {
		return node;
	}
}