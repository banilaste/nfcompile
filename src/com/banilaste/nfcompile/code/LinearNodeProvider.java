package com.banilaste.nfcompile.code;

import java.util.LinkedList;
import java.util.Stack;

import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.node.Node;
import com.banilaste.nfcompile.node.SeparatorNode;

public class LinearNodeProvider {
	private LinkedList<CodeLine> lines;
	private Stack<Node> returnedStack;
	
	private int lastLine = 0;
	private Node lastNode = null;
	
	public LinearNodeProvider(LinkedList<CodeLine> lines) {
		this.lines = lines;
		this.returnedStack = new Stack<Node>();
	}
	
	/**
	 * Return next node
	 * @return node fetched
	 * @throws NF4Exception 
	 */
	public Node next() throws NF4Exception {
		if (!hasNext()) {
			SeparatorNode.FILE_END.setLine(getLine());
			lastNode = SeparatorNode.FILE_END;
		}
		
		else if (!returnedStack.isEmpty()) {
			lastNode = returnedStack.pop();
		}
		
		else if (lines.element().canExtract()) {
			lastLine = lines.element().getLineNumber();
			
			lastNode = lines.element().next();
			lastNode.setLine(lastLine);
		}
		
		else {
			lines.remove();
			
			// We return a end line character
			SeparatorNode.LINE_END.setLine(getLine());
			lastNode = SeparatorNode.LINE_END;
		}
		
		return lastNode;
	}
	
	/**
	 * Return a node into the provider (unused or require after).
	 * @param node node to return
	 */
	public void returnNode(Node node) {
		returnedStack.push(node);
	}
	
	public boolean hasNext() {
		return !(lines.isEmpty() && returnedStack.isEmpty());
	}
	
	public int getLine() {
		return lastLine;
	}
	
	public String toString() {
		StringBuffer bf = new StringBuffer(this.getClass().getSimpleName());
		
		bf.append(" (");
		bf.append(this.lines.size());
		bf.append(" lines)");
		
		return bf.toString();
	}

	public Node getLast() {
		return lastNode;
	}
}