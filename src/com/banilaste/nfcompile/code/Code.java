package com.banilaste.nfcompile.code;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.banilaste.nfcompile.NodeBind;
import com.banilaste.nfcompile.code.NodeString.NodeStringMatcher;
import com.banilaste.nfcompile.exception.NF4Exception;
import com.banilaste.nfcompile.node.ConstantNode;
import com.banilaste.nfcompile.node.block.Program;
import com.banilaste.nfcompile.node.variable.StringNode;
import com.banilaste.nfcompile.type.Type;
import com.banilaste.nfcompile.type.Variable;
import com.banilaste.nfcompile.type.builtin.CharacterType;
import com.banilaste.nfcompile.type.builtin.FloatType;
import com.banilaste.nfcompile.type.builtin.IntegerType;
import com.banilaste.nfcompile.type.builtin.StringType;

public class Code {
	// Patterns matching constants
	private final static Pattern STRING_REGEX = Pattern.compile("\"(?<content>(?:[^\"]|(?:\\\"))*?)\""),
		CHAR_REGEX = Pattern.compile("'(?<content>(?:[^']{1})|(?:\\\\['0nt]))'"),
		FLOAT_REGEX = Pattern.compile("(?<start>[^a-zA-Z_\\.])(?<content>[0-9]*\\.[0-9]+)"),
		INTEGER_REGEX = RegexUtil.keyword("(?<content>[0-9]+)", "[^a-zA-Z_\\.]|(?:\\.\\.)", "");//"[^\\.]|(?:\\.\\.)");
	
	public final static Pattern COMMENTS = Pattern.compile("//.*$", Pattern.MULTILINE);
	public final static Pattern STRINGS = Pattern.compile("(?<start>[^a-zA-Z0-9_���])(?<string>[a-zA-Z_����]+[a-zA-Z0-9_����]*)");
	
	private String originalCode;
	
	public Code(String content) {
		originalCode = content;
	}
	
	public Program compile() throws NF4Exception {
		// Precompile program
		LinearNodeProvider provider = this.precompile();

		// Create algorithm forest
		Program p = Program.create();
		p.parse(provider, null);

		// Return parsed program
		return p;
	}
	
	/**
	 * Prepare code to be parsed as a tree
	 * @return code content provider
	 * @throws NF4Exception
	 */
	public LinearNodeProvider precompile() throws NF4Exception {
		// Remove single lines comments
		// TODO multiline comments, test for // inside strings
		NodeString code = new NodeString(COMMENTS.matcher(originalCode).replaceAll(""));

		// Parse constants inside the code
		parseConstants(code);

		// Parsing nodes
		parseNodes(code);
		
		// Extract strings
		parseStrings(code);
		
		// Split into lines
		LinkedList<NodeString> lines = code.splitLines();
		
		// Cleaning remaining white spaces and create associated code line
		LinkedList<CodeLine> codesLines = new LinkedList<CodeLine>();
		int line = 0;
		
		for (Iterator<NodeString> iter = lines.iterator(); iter.hasNext(); line++) {
			NodeString nodeString = iter.next().clean();
			
			// We add only non empty lines
			if (nodeString.getParts().size() > 0) {
				codesLines.add(new CodeLine(nodeString, line));
			}
		}
		
		// Return a code line provider
		return new LinearNodeProvider(codesLines);
	}
	
	/**
	 * Parse nodes insides code
	 * @param code source code
	 * @return code with nodes replaces by their id
	 */
	private void parseNodes(NodeString code) {
		NodeBind[] bindings = NodeBind.values();
		
		// Of every possible nodes
		for (int i = 0; i < bindings.length; i++) {

			// and those who can be parsed through regex
			if (bindings[i].getDescriptor() != null) {
				NodeBind bind = bindings[i];
				
				// we replace matched patterns with the right node
				code.replace(bindings[i].getDescriptor(), new NodeStringMatcher() {
					@Override
					public NodeStringPart replaceBy(Matcher matcher) {
						return new NodeStringPart(bind.create());
					}
				});
			}
		}
	}
	
	/**
	 * Extract strings from code
	 * @param code source code
	 * @return code with strings replaced by their id
	 */
	private void parseStrings(NodeString code) {
		code.replace(STRINGS, new NodeStringMatcher() {
			@Override
			public NodeStringPart replaceBy(Matcher matcher) {
				String string = matcher.group("string");
				
				// Then add the string node descriptor
				StringNode node = new StringNode(string);
				
				return new NodeStringPart(node);
			}
		});
	}
	
	/**
	 * Retrieve constants from code, store them, and replace with identifier
	 * @param code code content
	 * @return modified code
	 */
	private void parseConstants(NodeString code) {
		parseConstants(code, STRING_REGEX, StringType.TYPE);
		parseConstants(code, CHAR_REGEX, CharacterType.TYPE);
		parseConstants(code, FLOAT_REGEX, FloatType.TYPE);
		parseConstants(code, INTEGER_REGEX, IntegerType.TYPE);
	}
	
	private void parseConstants(NodeString code, Pattern regex, Type type) {
		code.replace(regex, new NodeStringMatcher() {
			@Override
			public NodeStringPart replaceBy(Matcher matcher) {
				Variable constant = new Variable(StringType.TYPE, matcher.group("content"));

				ConstantNode node = new ConstantNode(constant.castTo(type));

				return new NodeStringPart(node);
			}
		});
	}
}