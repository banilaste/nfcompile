package com.banilaste.nfcompile.code;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * String containing nodes
 * @author Clement
 *
 */
public class NodeString {
	private LinkedList<NodeStringPart> parts, emptyList;
	
	public NodeString() {
		parts = new LinkedList<NodeStringPart>();
		emptyList = new LinkedList<NodeStringPart>();
	}
	
	public NodeString(String content) {
		this();
		
		parts.add(new NodeStringPart(content));
	}
	
	/**
	 * Replace each occurrence by the string given in the callback
	 * @param regex regular expression
	 * @param callback callback called when an occurrence is found
	 */
	public synchronized void replace(Pattern regex, NodeStringMatcher callback) {
		LinkedList<NodeStringPart> parts = emptyList;
		
		for (Iterator<NodeStringPart> iter = this.parts.iterator(); iter.hasNext();) {
			NodeStringPart stringPart = iter.next();
			
			// Replace inside parts and add generated parts in the list
			stringPart.replace(regex, callback, parts);
			
			// Then remove element replaced (to empty list)
			iter.remove();
		}

		// Set parts as current collection
		emptyList = this.parts;
		this.parts = parts;
	}
	
	/**
	 * Split string into lines
	 * @return list of lines generated
	 */
	public LinkedList<NodeString> splitLines() {
		LinkedList<NodeString> lines = new LinkedList<NodeString>();
		NodeString string = new NodeString();
		
		for (Iterator<NodeStringPart> iterator = parts.iterator(); iterator.hasNext();) {
			NodeStringPart nodeStringPart = iterator.next();

			// if it's a string
			if (nodeStringPart.isString()) {
				String parts[];
				
				// We split as much lines as we can
				parts = nodeStringPart.getContent().split("\n");
				string.getParts().add(new NodeStringPart(parts[0]));
				
				for (int i = 1; i < parts.length; i++) {
					// We add the line
					lines.add(string);

					// Create new one
					string = new NodeString();
					
					// Then add the part
					string.getParts().add(new NodeStringPart(parts[i]));
				}
			} else {
				string.getParts().add(nodeStringPart);
			}
		}
		
		lines.add(string);

		return lines;
	}

	/**
	 * Remove empty strings and clean whitespace
	 * @return 
	 */
	public NodeString clean() {
		// We clean every part
		for (Iterator<NodeStringPart> iter = parts.iterator(); iter.hasNext();) {
			NodeStringPart part = iter.next();
			
			part.clean();
			
			// If the part is an empty string
			if (part.isString() && part.toString().length() == 0) {
				// We remove it
				iter.remove();
			}
			
		}
		
		return this;
	}
	
	/**
	 * Interface for replacement purpose
	 * @author Cl�ment
	 *
	 */
	interface NodeStringMatcher {
		public NodeStringPart replaceBy(Matcher matcher);
	}

	
	public LinkedList<NodeStringPart> getParts() {
		return parts;
	}
	
	public String toString() {
		StringBuffer bf = new StringBuffer();

		bf.append(this.getClass().getSimpleName());
		bf.append(" (");
		bf.append(this.getParts().size());
		bf.append(" parts)");
		
		return bf.toString();
	}
}
